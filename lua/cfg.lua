require 'lfs'
require 'lua.utils'

CWD = os.getenv('CWD') or os.getenv('CD')
PWD = os.getenv('PWD') or CWD

SYS_HOME = (qryenvs('SYS_HOME','SYSTEMDRIVE') or '')
HOME = (qryenvs('USERPROFILE','CSIDL_PROFILE','HOMEPATH','HOME') or '~')
TMP_HOME = (qryenvs('TMP_HOME','TMP_DIR','TEMP','TMP') or HOME .. '/.tmp~')

--[[ Since start menus etc are inconsistently supported (or not at all in some
cases), I'll support them via an internal start menu for which desktop plugins
can hook and show, I'm thinking of something like ~/.paw/*_menu/config.kson ]]

-- Optional extras
TOP_DIR  = (os.getenv('TOP_DIR') or PWD)
OTG_HOME = (os.getenv('OTG_HOME') or TOP_DIR)

-- Source code to0 be compiled
SRC_DIR  = (os.getenv('SRC_DIR') or 'src')
-- Headers for source code to be compiled
INC_DIR  = (os.getenv('INC_DIR') or 'include')

--[[	No more trying to identify the CPU or platform in makefiles, paw
	handles that for you ]]
local _ARCH64 = os.getenv('PROCESSOR_ARCHITEW6432')
local _ARCH32 = os.getenv('PROCESSOR_ARCHITECTURE')
local _WHICH = function()
	if _ARCH64 or _ARCH32 then return 'windows'
	else return (os.getenv('OS') or 'posix')
	end
end

OS = _WHICH()
CPU = (_ARCH64 or _ARCH32 or 'x86'):lower()

ARCH = (os.getenv('ARCH') or _ARCH64 or _ARCH32 or CPU):lower()

_ARCH64 = nil
_ARCH32 = nil
_WHICH = nil

--[[
> Variable indicates was booted under paw and which directories
> You should NOT set it yourself, doing so can lead to unwanted behaviour
]]
PAW_DIR = (os.getenv('PAW_DIR') or '.paw/debug/_+_+cc')
DST_ABI = (os.getenv('DST_ABI') or ARCH)
DST_API = (os.getenv('DST_API') or OS)

BIN_DIR = (os.getenv('BIN_DIR') or '${TOP_DIR}${PAW_DIR}/bin')
LIB_DIR = (os.getenv('LIB_DIR') or '${TOP_DIR}${PAW_DIR}/lib')
ODIR = (os.getenv('ODIR') or '${TOP_DIR}${PAW_DIR}/obj')

OUTPUT_DIR = ODIR .. '/output'
SHARED_DIR = ODIR .. '/shared'
STATIC_DIR = ODIR .. '/static'

cfg =
{
	binary =
	{
		pfx = '',
		ext = '.elf',
		obj_dir = 'binary',
		dst_dir = 'bin'
	},
	shared =
	{
		pfx = 'lib',
		ext = '.so',
		obj_dir = 'shared',
		dst_dir = 'lib'
	},
	static =
	{
		pfx = 'lib',
		ext = '.a',
		obj_dir = 'static',
		dst_dir = 'lib'
	},
	src_dirs = {},
	inc_dirs = {},
	dep_dirs = {},
	goals = {},
	ansi_c = false,
	pedantic = true,
	builds = { "binary", "shared", "static" },
	variants =
	{
		QUICK = { dst = "release", ext = "" },
		DEBUG = { dst = "debug", ext = ".debug" },
		TIMED = { dst = "timed", ext = ".timed" },
	},
	-- Strings to search for in the CC string, will force .o if found
	gnu_variants = { "gcc", "gnu", "llvm", "clang", "mingw", "cygwin" }
}

ARGS = mustbe( RUN_DIR, "string", "" )
RUN_DIR = mustbe( RUN_DIR, "string", "run" )
SRC_DIR = mustbe( SRC_DIR, "string", 'src' )
BIN_DIR = mustbe( RUN_DIR, "string", ".paw/debug/_+_+cc/bin" )
LIB_DIR = mustbe( SRC_DIR, "string", '.paw/debug/_+_+cc/lib' )
INC_DIR = mustbe( INC_DIR, "string", '${TOP_DIR}include' )

-- Method of creation will be worked out from the extension (if supported)
ZIP_EXT = mustbe( ZIP_EXT, "string", ".iso" )

VARIANTS = ''

for i,v in pairs( cfg.variants ) do
	v.ref = i
	v.def = (v.def or '_' .. i)
	cfg.variants[i] = v
	VARIANTS = VARIANTS .. ' ' .. i
end
