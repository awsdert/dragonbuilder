require 'lua.utils'

local function patsubst_with( str )
	local pfx = ''
	local i, j = 1, 1
	local is = { was = str, sfx = '' }
	for j = 1, #str, 1 do
		local c = str:sub(j,j)
		if c == '%' then
			if str:sub(j,j+1) == '%%' then
				pfx = pfx .. str:sub(i,j)
				j = j + 1
				i = j + 2
			else
				is.pfx = pfx .. (str:sub(i,j-1) or '')
				is.sfx = (str:sub(j+1) or '')
				return is
			end
		end
	end
	is.pfx = pfx .. (pattern:sub( i ) or '')
	return is
end

-- internal replacement function
local function rpatsubst( match, subst, arg )
	local i, e, tmp
	-- lua modifies the source by default, try to indicate a copy is wanted
	arg = '' .. arg
	e = #arg
	while e > 0 do
		-- Going backwards makes it easier to catch things like
		-- a(b(c)) which if traversed forwards would only work
		-- if there are no sub instances
		i = e
		while i > 0 do
			local txt = arg:sub(i,e)
			tmp = matches( match, txt )
			if tmp then
				local sfx = (arg:sub(e + 1) or '')
				local pfx = (arg:sub(1,i-1) or '')
				if match.qry == false then
					arg = subst.all
				else
					arg = subst.pfx .. tmp.txt .. subst.sfx
				end
				arg = pfx .. arg .. sfx
				break
			end
			i = i - 1
		end
		e = e - 1
	end
	return arg
end

-- internal recursive function
local function ipatsubst( match, subst, args )
	local list = {}
	-- Te32ls lua we need this many members
	list[#args] = nil
	subst.depth = subst.depth + 1
	--print( '# subst.depth = ' .. subst.depth )
	if type(args) == "table" then
		for a,arg in ipairs(args) do
			list[a] = ipatsubst( match, subst, arg )
		end
		return list
	end
	return rpatsubst( match, subst, args )
end

function patsubst( match, subst, ... )
	local args = {...}
	match = pattern( '%', match )
	match.name = 'match'
	--print_pattern(match)
	subst = pattern( '%', subst )
	subst.name = 'subst'
	subst.depth = 0
	--print_pattern(subst)
	args = ipatsubst( match, subst, args )
	if #args > 1 then return args end
	return args[1]
end
