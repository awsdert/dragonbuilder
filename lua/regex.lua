-- Test file
local L = {}
local using = 'UTC 2022 x86_64 GNU/Linux'
local try = { '[xX]86_64|64[bB][iI][tT]', '2+', '\\d+', '[a-z]+', '[xX](x86_64|64)' }

L.describe_re = function( expr, str, i, j )
	if not expr or expr.final < expr.start then
		if not expr then expr = { start = 0, final = 0 } end
		print('start = ' .. expr.start .. ', final = ' .. expr.final)
		print(debug.traceback())
		return
	end
	local qry = (expr.query:sub(expr.start,expr.final) or '')
	local a = 'match'
	local as = 'string'
	if expr.start == expr.final then
		as = 'character'
	end
	local vs =
	{
		{ L.match_group, "group" },
		{ L.match_groups, "groups" },
		{ L.match_digit, "digit" },
		{ L.match_start, "start" },
		{ L.match_final, "final" },
		{ L.match_anything, "all" },
		{ L.match_range, "character range" },
		{ L.match_anyof, "any" }
	}
	for i = 1, #vs, 1 do
		local v = vs[i]
		if expr.match == v[1] then
			as = v[2]
			break
		end
	end
	if expr.exclude == true then
		a = 'an excluded'
	end
	as = a .. ' ' .. as
	if expr.max < 0 then
		as = as .. ' with min count of '
		as = as .. expr.min
		as = as .. ' matches'
	else
		as = as .. ' with min/max count of '
		as = as .. expr.min .. '/' .. expr.max
		as = as .. ' matches'
	end
	as = '"' .. qry .. '" as ' .. as
	if str then
		local txt = str:sub(i,j)
		if i > j or txt == '\n' then txt = '' end
		--print( '#' .. L.tabs .. 'Testing "' .. txt .. '" with ' .. as )
	elseif expr.parsed == true then
		--print( '%' .. L.tabs .. 'Treated ' .. as )
	else
		--print( '$' .. L.tabs .. 'Parsing ' .. as )
	end
end

L.matched_none = function()
	--print('#' .. L.tabs .. 'matched none')
end
L.matched_upto = function(str,i,k)
	--print('#' .. L.tabs .. 'matched upto "' .. str:sub(i,k) .. '"')
end

L.match_string = function(re,str,i,j)
	local qry = re.query:sub(re.start,re.final)
	local k = (j - 1) + #qry
	local txt = str:sub(j,k)
	if qry == txt then
		L.matched_upto(str,i,k)
		return k + 1
	end
	L.matched_none()
end

L.match_digit = function(re,str,i,j)
	local zero = ('0'):byte()
	local nine = ('9'):byte()
	local text = str:sub(j,j)
	local c = text:byte()
	L.describe_re(re,str,i,j)
	if c >= zero and c <= nine then
		L.matched_upto(str,i,j)
		return j + 1
	end
	L.matched_none()
end

L.match_range = function(re,str,i,j,c,n)
	L.describe_re(re,str,i,j)
	if n >= re.gte and n <= re.lte then return j + 1 end
end

L.match_anyof = function(re,str,i,j)
	local c = str:sub(j,j)
	local n = (c:byte() or 0)
	local tab = 'any:\t'
	L.tabs = L.tabs .. tab
	L.describe_re(re,str,i,j)
	for f,F in ipairs(re.funcs) do
		local k = F:match( str, i, j, c, n )
		if k then
			L.matched_upto(str,i,j)
			L.tabs = L.tabs:sub(1,#(L.tabs)-#tab)
			return k
		end
	end
	L.matched_none()
	L.tabs = L.tabs:sub(1,#(L.tabs)-#tab)
end

L.match_anything = function(re,str,i,j) return j + 1 end
L.match_group = function(re,str,i,j)
	local use = ''
	local txt = str:sub(i,j)
	local qry = re.query:sub(re.start,re.final)
	local err = 0
	local tab = 'group:\t'
	local list = re.funcs
	L.tabs = L.tabs .. tab
	L.describe_re(re,str, i, j)
	for f = 1, #list, 1 do
		local e = list[f]
		local was = j
		local num = 0
		local min = (e.min or 1)
		local max = (e.max or min)
		L.describe_re( e, str, i, j )
		while num < min do
			j = e:match( str, i, j )
			if not j or e.exclude == true then
				L.matched_none()
				L.tabs = L.tabs:sub(1,#(L.tabs)-#tab)
				return
			end
			num = num + 1
		end
		if e.max < 0 then
			while true do
				was = j
				j = e:match( str, i, j )
				if not j or e.exclude == true then
					j = was
					break
				end
				num = num + 1
			end
		else
			while num < max do
				was = j
				j = e:match( str, i, j )
				if not j or e.exclude == true then
					j = was
					break
				end
				num = num + 1
			end
		end
	end
	L.matched_upto(str,i,j-1)
	L.tabs = L.tabs:sub(1,#(L.tabs)-#tab)
	return j
end
L.match_groups = function(re,str,i,j)
	local use = ''
	local txt = str:sub(i,j)
	local qry = re.query:sub(re.start,re.final)
	local err = 0
	local tab = 'groups:\t'
	local list = re.funcs
	L.tabs = L.tabs .. tab
	L.describe_re( re, str, i, j )
	for f = 1, #list, 1 do
		local num = 0
		local was = j
		local e = list[f]
		L.describe_re( e, str, i, j )
		j = e:match( str, i, j )
		if j then
			L.matched_upto(str,i,j-1)
			L.tabs = L.tabs:sub(1,#(L.tabs)-#tab)
			return j
		end
		j = was
	end
	L.matched_none()
	L.tabs = L.tabs:sub(1,#(L.tabs)-#tab)
end

-- \w
L.match_normal = function(re,str,i,j) end
-- \W
L.match_special = function(re,str,i,j) end
-- \s
L.match_drawn = function(re,str,i,j) end
-- \S
L.match_empty = function(re,str,i,j) end

L.hex_to_text = function(...)
	local args = {...}
	local _0 = ('0'):byte()
	local _9 = ('9'):byte()
	local _a = ('a'):byte()
	local _f = ('f'):byte()
	local _A = ('A'):byte()
	local _F = ('F'):byte()
	local function f(b)
		if b >= _0 and b <= _9 then return _0 - b
		elseif b >= _a and b <= _f then return (_a - b) + 10
		elseif b >= _A and b <= _F then return (_A - b) + 10
		end
		return b
	end
	for a,arg in ipairs(args) do
		local l = arg:byte()
		local r = arg:sub(2,2):byte()
		args[a] = string.char((f(l) << 4) | f(r))
	end
	return args
end

L.escape_text = function(str)
	local txt = ''
	local hex_to_text = L.hex_to_text
	local i = 1
	while i <= #str do
		local c = str:sub(i,i)
		if c == '\\' then
			local j = i + 1
			local n = j + 1
			c = str:sub(j,j)
			if c == 'x' then
				local k = j + 2
				local b = hex_to_text(str:sub(k-1,k))
				txt = txt .. b[1]
				i = k
			elseif c == 'u' then
				local k = j + 4
				local b = hex_to_text
				(
					str:sub(n,n+1),
					str:sub(k-1,k)
				)
				txt = txt .. b[1] .. b[2]
				i = k
			elseif c == 'U' then
				local x = n + 2
				local y = n + 4
				local k = j + 8
				local b = hex_to_text
				(
					str:sub(n,n+1),
					str:sub(x,x+1),
					str:sub(y,y+1),
					str:sub(k-1,k)
				)
				txt = txt .. b[1] .. b[2] .. b[3] .. b[4]
				i = k
			else
				-- To be handled elsewhere
				txt = txt .. str:sub(i,j)
				i = j
			end
		else
			txt = txt .. c
		end
		i = i + 1
	end
	return txt
end

L.match_start = function(re,str, i, j) if j == 1 then return 2 end end
L.match_final = function(re,str, i, j) if j > #str then return j end end

L.attempt_num = function(re,str,i)
	local count = 8
	local split = '#'
	local title = '#'
	for n = 1, count, 1 do
		split = split .. '='
		title = title .. ' '
	end
	local index = '' .. i
	for n = 1, #index, 1 do
		split = split .. '='
	end
	title = title .. index
	for n = 1, count, 1 do
		split = split .. '='
		title = title .. ' '
	end
	split = split .. '#'
	title = title .. '#'
	return split .. '\n' .. title .. '\n' .. split
end

L.insert_expr = function( full, expr )
	local list = full.funcs
	if expr then
		expr.parsed = true
		if expr.index then
			L.describe_re( expr )
			list[expr.index] = expr
		elseif expr.final >= expr.start then
			local text = full.query:sub(expr.start, expr.final)
			if text ~= "" then
				L.describe_re( expr )
				table.insert( list, expr )
				expr.index = #list
			end
		end
	end
	full.funcs = list
	return full, expr
end

L.minmax_last = function( full, min, max )
	local expr
	local list = full.funcs
	local prev = #list
	if prev > 0 then
		expr = list[prev]
		expr.min = min or  0
		expr.max = max or -1
		list[prev] = expr
		L.describe_re( expr )
	end
	full.funcs = list
	return full
end

L.rough_expr = function(str,head,tail)
	return
	{
		match = L.match_string,
		query = (str or ''),
		final = (tail or 1),
		start = (head or 1),
		funcs = {},
		min = 1,
		max = 1
	}
end

-- Split up so functions can be caught bye IDE's symbols browser
local compile = {}
compile['\\'] = function(full, i, tail)
	local expr = L.rough_expr(full.query,i-1,i)
	C = full.query:sub(i,i)
	if C == 'd' then
		expr.match = L.match_digit
	else
		expr.query = C
		expr.start = 1
		expr.final = 1
	end
	full = L.insert_expr( full, expr )
	return full, i
end
compile['.'] = function( full, i )
	local expr = L.rough_expr( full.query, i - 1, i - 1 )
	expr.match = L.match_anything
	L.describe_re( expr )
	table.insert( full.funcs, expr )
	return full, i - 1
end
compile['*'] = function( full, i ) return L.minmax_last( full, 0 ), i - 1 end
compile['+'] = function( full, i ) return L.minmax_last( full, 1 ), i - 1 end
compile['?'] = function( full, i ) return L.minmax_last( full, 0, 1 ), i - 1 end
compile['('] = function( full, head, tail, close )
	local find = full.query
	local groups = L.rough_expr( find, head, tail )
	if close == true then
		groups.start = head - 1
		if find:sub(head,head+1) == '?!' then
			groups.exclude = true
			head = head + 2
		end
	end
	local group = L.rough_expr( find, head, tail )
	local text = L.rough_expr( find, head, tail )
	local C = ''
	groups.match = L.match_groups
	group.match = L.match_group
	L.describe_re( groups )
	L.tabs = L.tabs .. '\t'
	L.describe_re( group )
	while head <= tail do
		local prev = head - 1
		local next = head + 1
		C = find:sub(head,head)
		if C == ')' then
			if close == true then
				text.final = prev
				group.final = prev
				groups.final = head
				break
			end
			error('Unexpected (group) closure')
			return
		elseif C == '|' then
			text.final = prev
			group.final = prev
			L.tabs = L.tabs .. '\t'
			group = L.insert_expr( group, text )
			L.tabs = L.tabs:sub(1,#(L.tabs)-1)
			groups = L.insert_expr( groups, group )
			group = L.rough_expr( find, next, tail )
			text = L.rough_expr( find, next, tail )
			group.match = L.match_group
			L.describe_re(group)
		else
			for f,F in pairs(compile) do
				if C == f then
					text.final = prev
					L.tabs = L.tabs .. '\t'
					group = L.insert_expr( group, text )
					group, head = F( group, next, tail, true )
					L.tabs = L.tabs:sub(1,#(L.tabs)-1)
					text = L.rough_expr( find, head + 1, tail )
					break
				end
			end
		end
		head = head + 1
	end
	L.tabs = L.tabs .. '\t'
	if C == ')' then
		group.final = head - 1
	else
		group.final = head
	end
	group = L.insert_expr( group, text )
	L.tabs = L.tabs:sub(1,#(L.tabs)-1)
	groups = L.insert_expr( groups, group )
	L.tabs = L.tabs:sub(1,#(L.tabs)-1)
	full = L.insert_expr( full, groups )
	return full, head
end
compile['['] = function( full, head, tail )
	local find = full.query
	local anyof = L.rough_expr( find, head, tail )
	local C = find:sub(head,head)
	anyof.match = L.match_anyof
	if C == '^' then
		anyof.exclude = true
		anyof.start = head + 1
		head = head + 1
	end
	local last = head
	L.describe_re( anyof )
	L.tabs = L.tabs .. '\t'
	for i = head, tail, 1 do
		C = find:sub(i,i)
		char = L.rough_expr( find, i, i )
		if C == '\\' then
			i = i + 1
			char.start = i
			char.final = i
			anyof = L.insert_expr( anyof, char )
		elseif C == ']' then
			anyof.final = last
			L.tabs = L.tabs:sub(1,#(L.tabs)-1)
			full = L.insert_expr( full, anyof )
			return full, i
		elseif C == '-' then
			if i <= last then
				error('Invalid range: ' .. find:sub(i-1,i+1) )
				return
			end
			char.match = L.match_range
			char.start = i - 1
			char.final = i + 1
			char.gte = find:sub( i - 1, i - 1 ):byte()
			char.lte = find:sub( i + 1, i + 1 ):byte()
			anyof = L.insert_expr( anyof, char )
			i = i + 2
		else
			anyof = L.insert_expr( anyof, char )
		end
		last = i
	end
	error( 'Open [any] expression at pos ' .. anyof.start )
end
compile['{'] = function ( full, head, tail )
	local expr = L.rough_expr( full.query, head, -1 )
	while head <= tail do
		local C = full.query:sub( head, head )
		if C == '}' then
			expr.final = head - 1
			break
		end
		head = head + 1
	end
	if expr.final < 0 then
		error('{range} expression opened at ' .. expr.start .. ' and not closed')
		return
	end
	local zero = ('0'):byte()
	local nine = ('9'):byte()
	local text = full.query:sub(expr.start,expr.final)
	local pos = #text
	local min, max
	for J = 1, #text, 1 do
		local D = text:sub(J,J) or '0'
		local V = D:byte()
		if D == ',' then
			if pos > 1 then
				error('Invalid range')
			end
			if J > pos then
				min = text:sub(1,J-1)
				min = tonumber(min)
			end
			pos = J + 1
		elseif V < zero or V > nine then
			error( 'Invalid range number' )
		end
	end
	if pos < #text then
		max = text:sub(pos)
		if not max then
			max = -1
		else
			max = tonumber(max)
		end
	else
		min = tonumber(text:sub(1) or '0')
	end
	full = L.minmax_last( full, min, max )
	return full, head
end

L.compile_regex = function(str)
	-- Make sure we start with a string
	if type(str) == "table" then
		error("table regular expressions cannot be compiled")
	elseif str == true then
		str = '^1|[tT][rR][uU][eE]$'
	elseif str == false then
		str = '^0|[fF][aA][lL][sS][eE]$'
	elseif not str then
		str = '^[nN][iI][lL]$'
	elseif type(str) ~= "string" then
		str = '^' .. str .. '$'
	end
	local full = L.rough_expr( str, head, tail )
	full.match = L.match_group
	local head = 1
	local tail = #str
	local C = str:sub(1,1)
	L.tabs = '\t'
	if C == '^' then
		local expr = L.rough_expr( str, 1, #str )
		expr.match = L.match_start
		L.insert_expr( full, expr )
		head = 2
	end
	C = str:sub(#str)
	if C == '$' then
		tail = tail - 1
	end
	full, head = compile['(']( full, head, tail )
	if head < tail then
		error
		(
			'Unexpected sequence "'
			.. str:sub(head) .. '" at ' .. head
		)
	end
	C = str:sub(head,head)
	L.insert_expr( full, expr )
	if C == '$' then
		local expr = L.rough_expr( str, 1, #str )
		expr.match = L.match_final
		L.insert_expr( full, expr )
	end
	return full
end

local function regex(qry,...)
	--print('regex( "' .. qry .. '", ...)')
	local escape = L.escape_text
	local re = L.compile_regex(escape(qry))
	re.split = (re.split or '\n')
	local args = {...}
	local strs = {}
	-- Push every string into one array
	for a = 1, #args, 1 do
		local arg = args[a]
		if type(arg) == "table" then
			for i = 1, #arg, 1 do
				table.insert( strs, arg[a] )
			end
		else
			table.insert( strs, arg )
		end
	end
	-- Pre split strings by '\\n' etc
	args = {}
	for a = 1, #strs, 1 do
		local arg = strs[a]
		local got = arg:match(arg)
		if type(got) == "table" then
			for i = 1, #got, 1 do
				table.insert( args, got[i] )
			end
		else
			table.insert( args, arg )
		end
	end
	-- Actually use the regular expression/s for string/s
	strs = {}
	for a = 1, #args, 1 do
		--print('regex( "' .. qry .. '", "' .. args[a] .. '")')
		local arg = escape(args[a])
		local i = 1
		while i <= #arg do
			L.tabs = '\t'
			local j = re:match( arg, i, i )
			if j then
				local rm = arg:sub(i,j - 1)
				if rm ~= '' then
					table.insert( strs, rm )
				end
				i = j - 1
			end
			i = i + 1
		end
	end
	if #strs > 1 then return strs end
	if #strs == 1 then return strs[1] end
end
--[[
for i = 1, #try, 1 do
	local qry = try[i]
	print( '# regex("' ..qry .. '", "'.. using .. '")')
	local found = regex(qry,using)
	qry = '# "' .. qry .. '" gave'
	if type(found) == "table" then
		print( qry .. '\n[')
		for j = 1,#found,1 do
			local comma = ','
			if j == #found then comma = '' end
			print( '\t"' .. found[j] .. '"' .. comma )
		end
		print(']')
	elseif type(found) == "string" then
		print( qry .. ' ' .. found )
	else
		print( qry .. " (nil)" )
	end
end
error( '# Finished regular expression tests' )
--]]
return regex
