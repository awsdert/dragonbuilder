require 'lfs'
require 'lua.cfg'
require 'lua.utils'
local regex = require('lua.regex')
_win32_expr = "[wW][iI][nN](64|32|[nN][tT])"
darwin_expr = "[dD][aA][rR][wW][iI][nN]|[oO][sS][xX]|[aA][pP][pP][lL][eE]"

function rmall(...)
	local text = ''
	local args = {...}
	for a,arg in ipairs(args) do
		local mode, err = lfs.attributes(arg, 'mode')
		if mode == 'directory' then
			for name in lfs.dir(arg) do
				if name ~= "." and name ~= ".." then
					text = text .. '\n'
						.. rmall(arg .. '/'.. name)
				end
			end
		end
		if err then error(err) end
		os.remove(arg)
		text = text .. '\nremoved "' .. arg .. '"'
	end
	return text
end

if os.getenv("COMSPEC") then
	uname_s = "WIN32"
	uname_p =
		(os.getenv("PROCESSOR_ARCHITEW6432") or
		os.getenv("PROCESSOR_ARCHITECTURE"))
	uname_a = uname_p
	DIR_SEP = '\\'
	PATH_SEP = ';'
	cfg.dir_sep = '\\'
	RM_f = 'del'
	RM_R = 'rd /s/q'
	MWD = '%TOP_DIR%'
	valpfx = ''
	cmdval = function(name) return '%' .. '%' end
else
	uname_s = shell( "uname -s" )
	uname_p = shell( "uname -p" )
	uname_a = shell( "uname -a" )
	RM_f = 'rm -f'
	RM_R = 'rm -rdv'
	MWD = '${TOP_DIR}'
	valpfx = '$'
	cmdval = function(name) return '${' .. '}' end
	if regex(darwin_expr,uname_s) then
		DIR_SEP = ':'
		PATH_SEP = ';'
	else
		DIR_SEP = '/'
		PATH_SEP = ':'
	end
end

local archs =
{
	{
		dst = 'ia64',
		qry = "[iI][aA]64",
		def = {"__IA64__","__x86_64__"}
	},
	{
		dst = 'amd64',
		qry = "[aA][mM][dD]64",
		def = {"__AMD64__","__x86_64__"}
	},
	{
		dst = 'x86_64',
		qry = "[xX](86_64|64)|64[bB][iI][tT]",
		def = "__x86_64__"
	},
	{
		dst = 'amd',
		qry = "[aA][mM][dD]",
		def = {"__AMD__","__x86__"}
	},
	{
		dst = 'x86',
		qry = "([xX]86)|(32[bB][iI][tT])",
		def = "__x86__"
	}
}

uname_s = uname_s:gsub('\n','')
uname_a = uname_a:gsub('\n','')
uname_p = uname_p:gsub('\n','')
if uname_p == "unknown" then
	uname_p = uname_a
end

CC		= mustbe( CC,		"string", "cc"		)
SYS		= mustbe( SYS,		"string", uname_s	)
ARCH		= mustbe( ARCH,		"string", uname_p	)
CFLAGS		= mustbe( CFLAGS,	"string", ""		)
CXXFLAGS	= mustbe( CXXFLAGS,	"string", ""		)
binary_flags	= mustbe( binary_flags,	"string", ""		)
shared_flags	= mustbe( shared_flags,	"string", ""		)
static_flags	= mustbe( static_flags,	"string", ""		)

cfg.arch = archs[#archs]
for i,v in ipairs(archs) do
	local got = regex( v.qry, ARCH )
	--print('Trying "' .. v.qry .. '" on "' .. ARCH .. '"' )
	if got then
		cfg.arch = v
		SYS_ABI = mustbe( SYS_ABI, "string", v.dst )
		break
	end
end

function __compile( o, target, c, source )
	return ' ' .. o .. ' "' .. target .. '" ' .. c .. ' "' .. source .. '"'
end

function declare_mscsource()
	OBJ_EXT = ".obj"
	STATIC_EXT = ".lib"
	cfg.src_def = "_MSC_SOURCE"
	WALL = "/Wall"
	function rpath(path) return '' end
	cfg.variants.DEBUG.flags = "/Debug"
	cfg.variants.QUICK.flags = "/O3"
	cfg.variants.TIMED.flags = "/Profile"
	cfg.binary.flags = binary_flags
	cfg.shared.flags = shared_flags
	cfg.static.flags = static_flags
	option = function(text) return "/" .. text end
	compile = function( target, source )
		return '/o "' .. target .. '"\t /c "' .. source .. '"' end
	precompile = function( target, source )
		return '/P "' .. target .. '"\t /E "' .. source .. '"' end
end

function declare_gnusource()
	OBJ_EXT = ".o"
	STATIC_EXT = ".a"
	cfg.src_def = "_GNU_SOURCE"
	WALL = "-Wall -Wextra -Werror"
	function rpath(path) return '-Wl,-rpath="' .. path .. '"' end
	if cfg.pedantic == true then
		WALL = WALL .. " -pedantic"
	end
	if cfg.ansi_c == true then
		CFLAGS = CFLAGS .. " -ansi"
		CXXFLAGS = CXXFLAGS .. " -ansi"
	end
	cfg.variants.DEBUG.flags = "-g"
	cfg.variants.QUICK.flags = "-O3"
	cfg.variants.TIMED.flags = "-pg"
	cfg.binary.flags = binary_flags .. '-fPIC'
	cfg.binary.build = function(obks,link)
		return CC .. ' -fPIC' .. obks .. ' ' .. link
	end
	cfg.shared.flags = shared_flags .. '-fPIC -shared'
	cfg.shared.build = function(obks,link)
		return CC .. ' -fPIC -shared' .. obks .. ' ' .. link
	end
	cfg.static.flags = static_flags .. '-fPIC'
	cfg.static.build = function(obks,link) return 'ar -rcs' .. obks end
	option = function(text) return "-" .. text end
	compile = function( target, source )
		return '-o "' .. target .. '"\t -c "' .. source .. '"' end
	precompile = function( target, source )
		return '-o "' .. target .. '"\t -E "' .. source .. '"' end
end

function declare_win32_sys()
	SYS_API = "windows"
	LIB_PFX = ""
	SHARED_EXT = ".dll"
	BINARY_EXT = ".exe"
	cfg.sys_def = "_WIN32"
	SYS_DEFINES = { cfg.sys_def }
	for i,v in ipairs(cfg.gnu_variants) do
		if holds( CC, v ) or holds( CXX, v ) then
			declare_gnusource()
			break
		end
	end
end

function declare_darwin_sys()
	SYS_API = "darwin"
	LIB_PFX = "lib"
	SHARED_EXT = ".dylib"
	BINARY_EXT = ".app"
	cfg.sys_def = "_Darwin"
	SYS_DEFINES = { cfg.sys_def }
end

function declare_linux_sys()
	SYS_API = "linux"
	LIB_PFX = "lib"
	SHARED_EXT = ".so"
	BINARY_EXT = ".elf"
	cfg.sys_def = "_linux"
	SYS_DEFINES = { "_posix", cfg.sys_def }
end

function declare_unix_sys()
	declare_linux_sys()
	table.insert( SYS_DEFINES, "_unix" )
end

if regex( _win32_expr, SYS ) then
	declare_mscsource()
	declare_win32_sys()
elseif regex( darwin_expr, SYS ) then
	declare_gnusource()
	declare_darwin_sys()
else
	declare_gnusource()
	declare_linux_sys()
end

function remove(path) return exec( RM_f .. '"' .. path .. '"' ) end
function remdir(path) return exec( RM_R .. '"' .. path .. '"' ) end

function onlydirs( ud ) return (ud.mode ~= "directory") end
function onlyfiles( ud ) return (ud.mode ~= "file") end

function modified(path)
	path = getenvs(path)[1]
	if exists(path) then
		return lfs.attributes( path, 'modification' )
	end
	return nil, "No such path"
end

function define(text) return option('D ' .. text) end
function lnklib(name) return option('l ' .. name) end
function linkto(path) return option('o "' .. path .. '"') end
function libdir(path) return option('L "' .. path .. '"') end
function incdir(path) return option('I "' .. path .. '"') end

function cwdent(path)
	return valpfx .. cwdval .. '/' .. path end
function cwdopt(opt,path)
	return option(opt .. ' "$(MWD)' .. path .. '"') end


if STDC then
	STDC = option("std=" .. STDC)
else
	STDC = "\t"
end
if STDCXX then
	STDCXX = option("std=" .. STDCXX)
else
	STDCXX = "\t"
end

cfg.cc_def = '_cc'
if regex( '[mM][iI][nN][gG][wW]', CC ) then
	cfg.cc_def = '_mingw'
	declare_gnusource()
elseif regex( '[lL][lL][vV][mM]|[cC][lL][aA][nN][gG]', CC ) then
	cfg.cc_def = '_llvm'
	declare_gnusource()
elseif regex( '[gG][cC][cC]', CC ) then
	cfg.cc_def = '_gcc'
	declare_gnusource()
end

SRC_API = mustbe( SRC_API, "string", SYS_API )
SRC_ABI = mustbe( SRC_ABI, "string", SYS_ABI )
DST_API = mustbe( DST_API, "string", SYS_API )
DST_ABI = mustbe( DST_ABI, "string", SYS_ABI )

cfg.build = DST_API .. '+' .. DST_ABI .. '+' .. CC
cfg.binary.ext = BINARY_EXT
cfg.shared.pfx = LIB_PFX
cfg.shared.ext = SHARED_EXT
cfg.static.pfx = LIB_PFX
cfg.static.ext = STATIC_EXT

quick_dir = DST_DIR .. '/' .. cfg.variants.QUICK.dst .. '/_+_+' .. CC
debug_dir = DST_DIR .. '/' .. cfg.variants.DEBUG.dst .. '/_+_+' .. CC
timed_dir = DST_DIR .. '/' .. cfg.variants.TIMED.dst .. '/_+_+' .. CC
QUICK_DIR = DST_DIR .. '/' .. cfg.variants.QUICK.dst .. '/' .. cfg.build
DEBUG_DIR = DST_DIR .. '/' .. cfg.variants.DEBUG.dst .. '/' .. cfg.build
TIMED_DIR = DST_DIR .. '/' .. cfg.variants.TIMED.dst .. '/' .. cfg.build

cfg.variants.DEBUG.dir = DEBUG_DIR
cfg.variants.QUICK.dir = QUICK_DIR
cfg.variants.TIMED.dir = TIMED_DIR

DEFINES = joinarrays( false, cfg.src_def, cfg.arch.def, SYS_DEFINES )
table.insert( DEFINES, 'LIB_PFX=' .. LIB_PFX )
table.insert( DEFINES, 'BINARY_EXT=' .. BINARY_EXT )
table.insert( DEFINES, 'SHARED_EXT=' .. SHARED_EXT )
for i,v in ipairs(DEFINES) do
	DEFINES[i] = define(v)
end
DEFINES = joinstrings( nil, DEFINES )

CFLAGS		= DEFINES .. " " .. CFLAGS
CXXFLAGS	= DEFINES .. " " .. CXXFLAGS
