require 'lfs'
local regex = require 'lua.regex'

function qryenvs( ... )
	local txt
	local args = {...}
	for i=1,#args,1 do
		txt = os.getenv(args[i])
		if txt then return txt end
	end
	return nil
end

function getenvs( ... )
	local args = {...}
	for i=1,#args,1 do
		args[i] = args[i]:gsub( '%${(.-)}', os.getenv )
	end
	return args
end

function print_pattern( is )
	print( '# pattern( "'.. is.wcc .. '", "' .. is.all .. '" )' )
	print( '# prefix identified as "' .. is.pfx .. '"' )
	print( '# suffix identified as "' .. is.sfx .. '"' )
	if is.qry == true then
		print( '# Expecting query' )
	else
		print( '# Not expecting query' )
	end
end

--[[ @example pattern( '%', '$(SRC_DIR)/%' )
	@note wcc is used as an escape character for itself so for the above
	example '%%' will be treated as a normal '%' character rather than the
	split point between prefix & suffix
]]
function pattern( wcc, str )
	if type(wcc) ~= 'string' or type(str) ~= 'string' then
		if type(wcc) ~= "string" then
			wcc = 'incorrect type("' .. type(wcc) .. '")'
		end
		if type(str) ~= "string" then
			str = 'incorrect type("' .. type(str) .. '")'
		end
		local err =
			'wildcard character was "' .. wcc .. '"'
			.. '\npattern string was "' .. str .. '"'
			.. '\nboth must be of string type'
		error(err)
		-- Just in case lua didn't abort the script
		return
	end
	if #wcc ~= 1 then
		error('wildcard must be ONE (1) character long!')
		return
	end
	local pfx = ''
	local esc = wcc .. wcc
	local i, j = 1, 1
	-- Ensure prefix & suffix are strings when we return the object
	local is = { wcc = wcc, all = str, pfx = '', sfx = '', qry = false }
	for j = 1, #str, 1 do
		local c = str:sub(j,j)
		if c == wcc then
			if str:sub(j,j+1) == esc then
				pfx = pfx .. str:sub(i,j)
				j = j + 1
				i = j + 2
			else
				is.pfx = pfx .. (str:sub(i,j-1) or '')
				is.sfx = (str:sub(j+1) or '')
				is.qry = true
				return is
			end
		end
	end
	is.pfx = pfx .. (str:sub( i ) or '')
	return is
end

-- @example matches( pattern(...), "..." )
function matches( match, str )
	if match.qry == false then
		if match.all == str then
			return { all = str, pfx = str, txt = '', sfx = '' }
		end
		return
	end
	local pfx = match.pfx
	local sfx = match.sfx
	local len = 1
	local pos = #str
	local init, text, ends = '', '', ''
	if pfx ~= '' then
		len = #pfx
		if len > #str then return end
		init = (str:sub(1,len) or '')
	end
	if sfx ~= '' then
		pos = #str - (#sfx - 1)
		if pos < len then return end
		ends = (str:sub(pos) or '')
	end
	text = (str:sub(len+1,pos-1) or '')
	if init == pfx and ends == sfx then
		return
		{
			all = init .. text .. ends,
			pfx = init,
			txt = text,
			sfx = ends
		}
	end
end

function subst( text, replace, with )
	if type(text) ~= "string" then return '' end
	if type(with) ~= "string" then with = '' end
	if type(replace) ~= "string" then replace = text end
	local str = ''
	while text ~= '' do
		local j = nil
		for i = 1, #text, 1 do
			local tmp = (text:sub(i,(i-1)+#replace) or '')
			if tmp == replace then
				j = i
				break
			end
		end
		if not j then return str .. text end
		str = str .. text:sub(1,j-1) .. with
		text = (text:sub(j + #replace) or '')
	end
	return str
end

function Print(...)
	local args = {...}
	for a,arg in ipairs(args) do
		print(arg)
	end
end

function info( ... )
	local msg, sep = '', ''
	local args = {...}
	for a,arg in ipairs(args) do
		msg = msg .. sep .. "$(info " .. arg .. ")"
		sep = '\n'
	end
	return msg
end
function Info( ... ) print( info( ... ) ) end
--[[	Some default functions just return these values, rather reallocate
	memory every time  they're need we just declare them here once ]]
function Nil() return nil end
function Boolify(v)
	if not v then
		return false
	end
	local list = { 0, "", "0", false, "false" }
	if type(v) == "string" then
		v = v:lower()
	end
	for _,i in ipairs(list) do
		if v == i then
			return false
		end
	end
	return true
end
function Zero() return 0 end
function True() return true end
function False() return false end
function Modify(...) return ... end
function exists(path)
	local ok, code, err = os.rename(path,path)
	if ok or code == 13 then return true, err end
	return nil, err
end
function shell(cmd)
	local o, err = assert(io.popen(getenvs(cmd)[1]))
	--o:flush()
	local out = assert(o:read('*a'))
	local sig = ({o:close()})[3]
	local msg = nil
	local signals = {}
	signals[2] = 'Uncaught interrupt'
	signals[6] = 'Program aborted'
	signals[9] = 'Program was forcefully terminated'
	signals[8] = "Division by 0, should equal 0 but mathmations are "
			.. "divided on what is very simple, source - 0 = "
			.. "add 0 to target (which starts as 0)"
	signals[11] = "Program tried to use a bad memory/function pointer"
	-- TODO: use named signals instead of "magic" numbers
	if sig ~= 0 then
		if signals[sig] then
			msg = signals[sig]
		else
			msg = 'Signal ' .. sig .. ' returned'
		end
	end
	return out, sig, msg
end
function exec(cmd,cb)
	print(cmd)
	local out, sig, msg = shell(cmd)
	if not cb then cb = print end
	if not sig or sig == 0 then return out, nil, msg end
	msg = (msg or '')
	cb( out .. '\n' .. msg .. '\n' )
	return out, sig, msg
end

function mkdir(dir,func)
	local c = dir:sub(#dir)
	if c == '/' or c == '\\' then dir = dir:sub(1,#dir-1) end
	if exists(dir) then return end
	exec('mkdir "' .. dir .. '"',func)
end

function cwdto( dir, path )
	if path:sub(1,1) == '/'
	or path:sub(1,2) == '~/'
	or path:find(":\\")
	or path:find("://")
		then return path
	elseif path:sub(1,2) == './'
		then return dir .. path:sub(3)
	end
	return dir .. path
end

function cwdtotop( path ) return cwdto( TOP_DIR, path ) end

function If( bool, ontrue, onfalse )
	if bool == true then return ontrue() end return onfalse() end
function strlen( str )
	if type(str) == "string" then return #str end return 0 end
function length( obj ) if not obj then return 0 end return #obj end

function mustbe( Data, Type, Subst )
	local function ontrue() return Data end
	local function onfalse() return Subst end
	return If( type(Data) == Type, ontrue, onfalse )
end

function mustbelistof( Data, Type, Subst )
	if type(Data) == Type and Type ~= "table" then
		Data = { Data }
	else
		Data = mustbe( Data, "table", {} )
	end
	for i,v in ipairs(Data) do
		Data[i] = mustbe( v, Type, Subst )
	end
	return Data
end

function tonum(val)
	local v = tonumber(val)
	if not v then return 0 end return v
end

function tostr(val,sub)
	local v = tostring(val)
	if type(sub) ~= 'string' then sub = '(nil)' end
	if not v or v == 'nil' then return sub end return v
end

function clonetable( t )
	local T = {}
	for i,v in pairs(t) do
		if type(v) == "table" then
			T[i] = clonetable(v)
		else
			T[i] = v
		end
	end
	for i,v in ipairs(t) do
		if type(v) == "table" then
			T[i] = clonetable(v)
		else
			T[i] = v
		end
	end
	return T
end

function joinobjects( ... )
	local obj = {}
	local args = {...}
	for a,arg in ipairs(args) do
		for i,v in pairs(arg) do
			obj[i] = v
		end
	end
	return obj
end

local function ijoinarrays( dive, all, args )
	-- Allocation optimisation
	all[#all + #args] = nil
	if dive == false then
		for a,arg in ipairs(args) do
			table.insert( all, arg )
		end
		return all
	end
	for a,arg in ipairs(args) do
		if type(arg) == "table" then
			for l,list in ipairs(arg) do
				all = ijoinarrays( true, all, list )
			end
		else
			table.insert( all, arg )
		end
	end
	return all
end

function joinarrays( dive, ... )
	local args = {...}
	local all = {}
	if type(dive) ~= "boolean" then
		error('API changed, expect boolean for question "Do we include sub tables as arrays?"')
	end
	for a,arg in ipairs(args) do
		if type(arg) == "table" then
			all = ijoinarrays( dive, all, arg )
		else
			table.insert( all, arg )
		end
	end
	return all
end

function joinstrings( config, ... )
	local str, sep, k, tmp = "", "", 0, ""
	config = mustbe( config, "table", {} )
	config.dive = mustbe( config.dive, "boolean", true )
	config.sep = mustbe( config.sep, "string", " " )
	config.name = mustbe( name, "string", "string" )
	config.convert = mustbe( config.convert, "function", Modify )
	local args = joinarrays( config.dive, ... )
	for i,arg in ipairs(args) do
		if arg ~= nil and type(arg) ~= "table" then
			k, tmp = config.convert(0,arg)
			str = str .. sep .. tostr(tmp,'')
			sep = config.sep
		end
	end
	return str
end

function add_unique_string( list, str )
	for i,v in ipairs(list) do
		if v == str then
			return list
		end
	end
	table.insert(list,str)
	return list
end

function hasext(path,ext)
	local regex = ".*." .. ext .. "$"
	local found = string.match( path, regex )
	if found and #found > 0 then
		return true
	end
	return false
end
function lacksext(path,ext) return (hasext(path,ext) == false) end

function filter(funcs, Break, ud)
	local leng, give
	leng = #funcs
	if type(Break) == "function" then
		for i = leng, 1, -1 do
			give = funcs[i](ud)
			if Break(give) == true then
				break
			end
		end
	else
		for i = leng, 1, -1 do
			give = funcs[i](ud)
			if give == Break then
				break
			end
		end
	end
	return give
end

function qsort(list,func,config)
	local leng = length(list)
	local moved = true
	local a, b, j, v, strx, stry
	local k = 0
	while moved do
		k = k + 1
		moved = false
		for i = 1, leng, 1 do
			x = nil
			j = i + 1
			v = list[i]
			if j <= leng then
				x = list[j]
				a, b = func( v, x, config )
				list[i] = a
				list[j] = b
			else
				a, b = func( v, nil, config )
				list[i] = a
			end
			if a ~= v then moved = true end
		end
	end
	return list
end

function moreismore( a, b, nilismore )
	a = tonum(a)
	config = mustbe( config, "", {} )
	if b ~= nil then return a - tonum(b) end
	if nilismore == true and a >= 0 then return -1 end
	return a
end
function lessismore( a, b, nilisless )
	a = tonum(a)
	config = mustbe( config, "table", {} )
	if b ~= nil then return -(a - tonum(b)) end
	if nilisless == true and a >= 0 then return 1 end
	return -a
end
function cmpwithdigits( a, b, config )
	local zero = ('0'):byte(1)
	local nine = ('9'):byte(1)
	local adig = a >= zero and a <= nine
	local bdig = b >= zero and b <= nine
	local diff
	config = mustbe( config, "table", {} )
	config.cmp = mustbe( config.cmp, "function", moreismore )
	diff = If
	(
		adig == bdig,
		function () return config.cmp( a, b, config.ud ) end,
		function () return config.cmp( adig, bdig, config.ud ) end
	)
	if diff <= 0 then
		return a, b
	end
	return b, a
end

function cmpstrings(a,b,config)
	local alen = strlen(a)
	local blen = strlen(b)
	local leng = If
	(
		alen < blen,
		function() return alen end,
		function() return blen end
	), val
	config = mustbe( config, "table", {} )
	config.cmp = mustbe( config.cmp, "function", moreismore )
	for i = 1, leng, 1 do
		val = config.cmp(a:byte(i), b:byte(i),config.ud)
		if val > 0 then return a, b elseif val < 0 then return b, a end
	end
	if not a then alen = nil end
	if not b then blen = nil end
	if config.cmp( alen, blen, config.ud ) > 0 then return a, b end
	return b, a
end

function sortstrings(dive,uniq,...)
	dive = mustbe( dive, "boolean", true )
	uniq = mustbe( uniq, "boolean", false )
	local args = joinarrays( dive, ... )
	local moved = true
	local i, j, k, stop
	while moved == true do
		moved = false
		i = 1
		for j = 2, #args, 1 do
			local a, b = cmpstrings( args[i], args[j] )
			moved = (moved == true or a ~= args[i])
			args[i] = a
			args[j] = b
			i = i + 1
		end
	end
	if uniq == false then return args end
	i = 1
	stop = #args
	while i <= stop do
		j = i + 1
		if j > stop then break end
		if args[j] == args[i] then
			k = j + 1
			while k <= stop do
				if args[k] ~= args[i] then
					break
				end
				k = k + 1
			end
			while k <= stop do
				args[j] = args[k]
				k = k + 1
				j = j + 1
			end
			k = j
			while k <= stop do
				args[k] = nil
				k = k + 1
			end
			stop = stop - (stop - j)
		end
		i = i + 1
	end
	return args
end

function cutstr( str, by ) return string.sub( str, 1, #str - by ) end
