EQUALS_LINE:====================================================================
HYPHEN_LINE:=-------------------------------------------------------------------
MAKE:=$(MAKE)$(if $(filter --no-print-directory,$(MAKE)),, --no-print-directory)

STDC?=c11
check?=paw
print=$1 holds $2
printm=$(call print,$1,$($1))
prints=$(call print,$1,"$($1)")
# We consider this the "workspace" variable so force it
TOP_DIR?=./
# We don't want relative paths since they don't play nice in editors that
# support clicking on the paths in errors & warnings produced
TOP_DIR:=$(abspath $(TOP_DIR))

ON_WINDOWS:=$(if $(PROCESSOR_ARCHITECTURE),1)

V=$(if $(ON_WINDOWS),%$$1%,$${$1})

MWD:=$(call V,TOP_DIR)
SYS_DIR:=$(abspath $(or $(SYSTEMDRIVE),/))
USR_DIR:=$(abspath $(or $(USERPROFILE),$(HOME),~/))
OTG_DIR:=$(abspath $(or $(OTG_DIR),$(TOP_DIR)))
EXT_PTY:=$(abspath $(or $(EXT_PTY),$(TOP_DIR)/3rdparty))

SYS_DIR:=$(patsubst %\%,%/%,$(SYS_DIR))
USR_DIR:=$(patsubst %\%,%/%,$(USR_DIR))
OTG_DIR:=$(patsubst %\%,%/%,$(OTG_DIR))
EXT_PTY:=$(patsubst %\%,%/%,$(EXT_PTY))

WALL?=-Wall -Wextra -pedantic -Werror
CFLAGS=$(WALL) $(DEFINES)
IFLAGS?=-I "$$$(call V,TOP_DIR)/include" -I "$$$(call V,EXT_PTY)/cglm/include"

CC?=cc
CC32?=i686-w64-mingw32-cc
CC64?=x86_64-w64-mingw32-cc
CC:=$(CC) -fPIC
CC32:=$(CC32) -fPIC
CC64:=$(CC64) -fPIC
RM?=rm -f
RM_DIR?=rm -Rf

inc_library=-I "$1/include" -L "$1/lib"

build_posix:=-D __posix__
build_unix:=-D __unix__ $(build_posix)
build_linux:=-D __linux__ $(build_unix)
build_macos:=-D Macintosh -D macintosh
build_apple:=-D __APPLE__
build_win16:=-D _WIN16 -D WINAPI_FAMILY=3 -mabi=ms
build_win32:=-D _WIN32 -D WINAPI_FAMILY=3 -mabi=ms -m32
build_win64:=-D _WIN32 -D _WIN64 -D WINAPI_FAMILY=3 -mabi=ms -m64

link_libs=-L "$$$(call V,TDIR)" $$(LFLAGS) $$(DEPS:%=-l% $$1) $$(LIBS:%=-l% $$1)
link=$$$(call V,$$1) $$2 -o "$$$(call V,TDIR)/$$3" $$4 $$(5:%="$$$(call V,ODIR)/%") $$6
compile=$$$(call V,$$1) $$2 -D build_$$3_$$(DECL) $$(IFLAGS) -o "$$$(call V,ODIR)/$$4" -c "$$$(call V,HERE)/$$5"
build_a=$(AR) -rcs

SRC_API?=$(if $(PROCESSOR_ARCHITECTURE),windows,linux)
SRC_API:=$(SRC_API)
SRC_ABI?=lp64
SRC_ABI:=$(if $(filter-out x86 ilp32,$(SRC_ABI)),$(SRC_ABI),x86-ilp32)
SRC_DIR:=$(abspath src)

DST_API:=$(if $(filter auto,$(DST_API)),$(SRC_API),$(DST_API))
DST_ABI:=$(if $(filter auto,$(DST_ABI)),$(SRC_ABI),$(DST_ABI))

subwild=$(strip $(foreach i,$(wildcard $1/$2),$(wildcard $i/$2)))
list_all=$(filter-out $(call subwild,$1,$2),$(wildcard $1/$2))
list_dirs=$(strip $(foreach i,$(call list_all,$1,*),$(if $(wildcard $i/*),$i)))
list_ents=$(strip $(foreach i,$(call list_all,$1,*),$(if $(wildcard $i/*),,$i)))
a32lsubdir=$(foreach i,$1,$i $(if $(call list_dirs,$i),$(call a32lsubdir,$(call list_dirs,$i))))
PRJ_DIRS:=$(call list_dirs,src)
PROJECTS:=$(PRJ_DIRS:src/%=%)
SRC_DIRS:=$(PRJ_DIRS) $(call a32lsubdir,$(PRJ_DIRS))

# The user might want things built elsewhere, perhaps to protect an SSD
DST_DIR?=$(TOP_DIR)
# Avoid conflicts by just putting everything in a subfolder but support using
# one the user specifies
filter_paw=$(abspath $(if $(filter .paw,$(notdir $1)),$(notdir $1),$1))
DST_DIR:=$(call filter_paw,$(DST_DIR))

.DEFAULT?=paw

abi_path=$5/.paw/$(if $4,$4/)$(if $(and $1,$2,$3),$1+$2+$3,_)
DST_PATH:=$(call abi_path,$(DST_API),$(DST_ABI),$(CC),$(OBJ_TYPE),$(DST_DIR))

__CC_DIRS=$1 $1/prks $1/objs $(SRC_DIRS:src/%=$1/objs/%)

_ABI_DIRS=$(if $4,$5/.paw/$4) $(call __CC_DIRS,$(call abi_path,$1,$2,$3,$4,$5))
DEST_DIRS=$(call _ABI_DIRS,$(DST_API),$(DST_ABI),$(CC),$1,$(DST_DIR))
LIST_DIRS=$(call _ABI_DIRS,,,$(CC),$1,$(DST_DIR))
DST_DIRS:=$(DST_DIR)
DST_DIRS+=$(DST_DIR)/.paw
DST_DIRS+=$(DST_DIR)/.paw/_
DST_DIRS+=$(call DEST_DIRS,)
DST_DIRS+=$(call DEST_DIRS,debug)
DST_DIRS+=$(call DEST_DIRS,timed)
DST_DIRS+=$(call DEST_DIRS,release)


# This is PAW terrain now so force the variables to be match what is expected
BIN_DIR:=$(DST_ABI)+$(CC)/bin
LIB_DIR:=$(DST_ABI)+$(CC)/lib

# Unsure if this is the right variable, csc is the modern variant of mcs
# CS?=csc
# CS?=mcs
# Example:
# $(CS) x.cs
# mono x.exe

CMD=$(info $1) $(info $(shell $1))

# Cleanup previous outputs
#$(if $(wildcard $(DECLARED)),$(call CMD,$(RM) $(DECLARED)))
addvar=$(if $($1),echo $1 = "'$($1)'" >> "$(DECLARED)")

git_pull=$(if $(wildcard $($1)/$2),cd "$(call V,$1)/$2" && git pull,cd "$(call V,$1)" && git clone "$3")
pull_ext=$(call $1_pull,EXT_PTY,$2,$3)

.EXPORT_ALL_VARIABLES:

all: build

pull:
	$(call pull_ext,git,cglm,https://github.com/recp/cglm.git)
	$(call pull_ext,git,paw,https://gitlab.com/awsdert/paw.git)
	$(call pull_ext,git,check,https://github.com/libcheck/check.git)
	@echo Done pulling/cloning

rebuild: clean all

SPLINT_OPTS:=+posixlib -type -boolops -predboolothers -predboolint
SPLINT_OPTS+=-shiftimplementation -fullinitblock -temptrans -usereleased
SPLINT_OPTS+=-dependenttrans -statictrans -nullpass -nullret -nullassign
SPLINT_OPTS+=-compdef -globstate -compdestroy -observertrans -mustfreefresh
SPLINT_OPTS+=-mustfreeonly -immediatetrans -unqualifiedtrans -freshtrans
SPLINT_OPTS+=-exportlocal

splint:
	splint $(SPLINT_OPTS) $(TOP_DIR)include/paw/*.h

build: $(PROJECTS)

# Want to fix the name issue between versions of libc and mscrt by just mapping
# to a common name that's known to be just a symlink to the local variant, need
# to research more on how best to do this though
#libpawc: libpawc.so

#libpawc.so:
	#$(if $(wildcard /usr/lib64/libc.6.so),ln -T $(USR_DIR)/.paw/public/linux-x64/libpawc.so)

CLEAN_DIR?=$(DST_DIR)/.paw
CLEAN_DIR:=$(abspath $(CLEAN_DIR))

fullclean: clean
	$(MAKE) CLEAN_DIR="$(EXT_PTY)" clean

# They're all the same here anyways
clean clobber: mostlyclean
	$(RM_DIR) $(CLEAN_DIR)

mostlyclean: $(wildcard $(CLEAN_DIR)/*)
	$(if $(wildcard $</*),$(MAKE) CLEAN_DIR="$<" clean)
	$(RM) $</*

$(PROJECTS): $(DST_DIRS:%=%/)
	@echo $(EQUALS_LINE)
	cd src/$@ && $(MAKE) TDIR="$(call V,DST_PATH)/prks" ODIR="$(call V,DST_PATH)/objs/$@" -f build.mak all
	@echo $(EQUALS_LINE)

$(DST_DIR)/%/:
	mkdir $(DST_DIR)/$*

.PHONY: all info
.PHONY: clean clobber mostlyclean rm_tmp
.PHONY: run test gede seer debug valgrind cppcheck
.PHONY: build rebuild $(PROJECTS)
