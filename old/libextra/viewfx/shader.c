#include <basic/_/_buffer.h>
#include <basic/buffer/_/_text.h>
#include <extra/viewfx/_/_vfxvai.h>

dint _TERM_VFXTYPE_SHADER( VFXREF *S )
{
	BUFFER *Paths;
	uint i;
	void **paths;
	dint err = 0;

	if ( !S )
		return 0;

	Paths = S->Misc;
	if ( Paths )
	{
		paths = SeekVoidsb( Paths );

		for ( i = Paths->count; i; )
		{
			err = VoidAchs( (ACHS**)(paths + (--i)) );
			if ( err )
			{
				Printf( XMSG_ERRNO(,err) );
				return err;
			}
			Paths->count--;
		}

		VoidVoids( &(S->Misc) );
	}

	TermAchs( S->Buff );
	TermAchs( S->Name );

	return 0;
}

SHARED_EXP ACHS*	SeekShaderName( VFXREF *S ) { return S->Name; }
SHARED_EXP ACHS*	SeekShaderCode( VFXREF *S ) { return S->Buff; }
SHARED_EXP ACHS**	SeekShaderPATH( VFXREF *S, uint i )
	{ return (ACHS**)SeekVOID( S->Misc, i ); }
SHARED_EXP ACHS*	SeekShaderPath( VFXREF *S, uint i )
	{ return SeekVoid( S->Misc, i ); }

SHARED_EXP dint InitShader( VFXREF *shader, VFXDEST dest )
{
	dint err;
	LOCK *lock = &(shader->usual->lock);
	if ( shader->type )
		return FailVfxRefAlreadyInitialised( shader );
	if ( !dest || dest >= VFXDEST_SHADERS_COUNT )
	{
		err = EINVAL;
		Printf( XMSG_ERRNO(,err) );
		Puts
		(
			"Shader type must not be 0 && must be less than "
			"VFXDEST_SHADERS_COUNT!\n"
		);
		return err;
	}
	if ( !(shader->bound && shader->bound->type == VFXTYPE_GFXAPP) )
	{
		err = ENOTCONN;
		Printf( XMSG_ERRNO(,err) );
		Puts( "Must call MarkVfxRefBound( shader, vfxapp ) first!" );
		return err;
	}
	err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	shader->dest = dest;
	shader->type = VFXTYPE_SHADER;
	err = vfxvai.initVfxRefCB[VFXTYPE_SHADER]( shader );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		shader->type = 0;
		shader->dest = 0;
	}
	FreeLock( lock );
	return err;
}

SHARED_EXP VFXREF* FindShader( achs name, achs path )
{
	uint i, count = SeekNumObjects();
	OBJECT **objects = SeekAllObjects();
	uint leng = achsnot0( VFXREF_DESC );

	for ( i = 0; i < count; ++i )
	{
		OBJECT *object = objects[i];
		VFXREF *shader = NULL;
		ACHS *Text = NULL;
		achs desc = NULL;
		uint not0 = 0;

		if ( !object )
			continue;

		desc = SeekObjectType( object );
		not0 = achsnot0( desc );

		if ( achsfindn( desc, not0, VFXREF_DESC, leng ) != desc )
			continue;

		shader = SeekObjectData( object );

		if ( !shader || shader->type != VFXTYPE_SHADER )
			continue;

		if ( name )
		{
			Text = SeekShaderName( shader );
			if ( !achsdiff( Text->array, name ) )
				return shader;
		}

		if ( path )
		{
			Text = SeekShaderPath( shader, 0 );
			if ( Text && !achsdiff( Text->array, path ) )
				return shader;
		}
	}

	return NULL;
}

SHARED_EXP VFXREF* SeekShader( uint i )
{
	VFXREF *R = SeekVfxRef( i );
	return (R->type == VFXTYPE_SHADER) ? R : NULL;
}
