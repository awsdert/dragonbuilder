#include <extra/viewfx/_/_vfxvai.h>

VFXVAI	vfxvai = {NULL};
ACHS	vfxvai_log = {0};
COMMON	vfxvai_usual = {0};

uint VFX_COLOR_BIT			= 0;
uint VFX_DEPTH_BIT			= 0;

SHARED_EXP achs SeekVfxSourceName( dint source )
	{ return vfxvai.seekVfxSourceNameCB( source ); }
SHARED_EXP achs SeekVfxDefectName( dint defect )
	{ return vfxvai.seekVfxDefectNameCB( defect ); }
SHARED_EXP achs SeekVfxFromIdName( dint fromid )
	{ return vfxvai.seekVfxFromIdNameCB( fromid ); }
SHARED_EXP achs SeekVfxWeightName( dint weight )
	{ return vfxvai.seekVfxWeightNameCB( weight ); }
SHARED_EXP dint MoveVfxBox( dint x, dint y, dint h, dint w )
	{ return vfxvai.moveVfxBoxCB( x, y, h, w ); }
SHARED_EXP dint MarkVfxBox( fnum r, fnum g, fnum b, fnum a )
	{ return vfxvai.markVfxBoxCB( r, g, b, a ); }
SHARED_EXP dint ZeroVfxBit( uint opts )
	{ return vfxvai.zeroVfxBitCB( opts ); }

dint appTermVfxRef( VFXREF *vfxref ) { (void)vfxref; return 0; }

SHARED_EXP dint InitVfxVai( void *ud, MODULE *mod )
{
	dint err = 0;
	ACHS	*Log	= &vfxvai_log;
	COMMON	*usual	= &vfxvai_usual;
	vfxvai_log.usual = usual;
	vfxvai.common = usual;
	TakeLock( &(usual->lock) );
	do
	{
		initVfxVai_cb init = NULL;

		if ( usual->ud )
		{
			err = EADDRINUSE;
			break;
		}

		usual->ud = ud;
		err = InitAchsb( Log, "vfxvai log initialised!\n" );
		if ( err )
			break;

		init = FindSymbol( mod, "libInitVfxVai" );
		err = init ? init( &vfxvai ) : EADDRNOTAVAIL;
		if ( err )
		{
			Log2VfxVaif
( XMSG_ERRNO( "Failed to initialise wrapped api!\n", err ) );
			break;
		}

		initVfxDefsArray( mod );
		vfxvai.termVfxBufCB[0] = appTermVfxRef;
		termVfxObjCB[0] = _TERM_VFXTYPE_OTHERS;
		termVfxObjCB[VFXTYPE_GFXAPP] = _TERM_VFXTYPE_GFXAPP;
		termVfxObjCB[VFXTYPE_SHADER] = _TERM_VFXTYPE_SHADER;
		termVfxObjCB[VFXTYPE_CONFIG] = _TERM_VFXTYPE_CONFIG;
		termVfxObjCB[VFXTYPE_SYMBOL] = _TERM_VFXTYPE_SYMBOL;
		err = Log2VfxVaib(
			"Initialised vfxvai!\n===========================\n" );
	}
	while (0);
	FreeLock( &(usual->lock) );
	return err;
}

SHARED_EXP dint TermVfxVaiLog()
{
	if ( vfxvai.common )
		return EADDRINUSE;
	return TermAchs( &(vfxvai_log) );
}
SHARED_EXP dint TermVfxVai( MODULE *mod )
{
	dint err = 0;
	(void)mod;
	if ( !(vfxvai.common) )
		return 0;
	TakeLock( &(vfxvai_usual.lock) );
	do
	{
		err = VoidVfxRefs();
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}
		err = vfxvai.termVfxVaiCB( &vfxvai );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}
		memset( &vfxvai, 0, sizeof(VFXVAI) );
		VFX_COLOR_BIT = 0;
		VFX_DEPTH_BIT = 0;
	}
	while (0);
	FreeLock( &(vfxvai_usual.lock) );
	return err;
}

SHARED_EXP dint InitVfxDbg( bool insync, infoVfx_cb func, void *ud )
{
	dint err;
	TakeLock( &(vfxvai_usual.lock) );
	err = vfxvai.initVfxDbgCB( insync, func, ud );
	FreeLock( &(vfxvai_usual.lock) );
	return err;
}
SHARED_EXP dint TermVfxDbg( bool unsync )
{
	dint err;
	if ( !(vfxvai.termVfxVaiCB) )
		return 0;
	TakeLock( &(vfxvai_usual.lock) );
	err = vfxvai.termVfxDbgCB ? vfxvai.termVfxDbgCB( unsync ) : 0;
	FreeLock( &(vfxvai_usual.lock) );
	return err;
}

SHARED_EXP dint SeekVfxRefLog( ACHS *dst, VFXREF *R )
{
	LOCK *lock = R ? &(R->usual->lock) : &(vfxvai_usual.lock);
	dint err = TakeLock( lock );
	if ( err )
		return err;
	if ( R )
		err = vfxvai.seekVfxLogCB[R->type]( R, dst );
	else
	{
		ACHS *Log = &(vfxvai_log);
		err = MarkAchsn( dst, Log->array, Log->count );
		Log->count = 0;
	}
	FreeLock( lock );
	return err;
}


SHARED_EXP dint Log2VfxVaib( achs text )
	{ return GrowAchsb( &(vfxvai_log), text ); }
SHARED_EXP dint Log2VfxVain( achs text, uint not0 )
	{ return GrowAchsn( &(vfxvai_log), text, not0 ); }
SHARED_EXP dint Log2VfxVaiv( achs args, va_list va )
	{ return GrowAchsv( &(vfxvai_log), args, va ); }
SHARED_EXP dint Log2VfxVaif( achs args, ... )
{
	dint err;
	va_list va;
	va_start( va, args );
	err = Log2VfxVaiv( args, va );
	va_end( va );
	return err;
}
