#include <basic/_/_buffer.h>
#include <extra/viewfx/_/_vfxvai.h>

dint _TERM_VFXTYPE_GFXAPP( VFXREF *vfxapp )
{
	dint err = 0;
	if ( !vfxapp )
		return 0;
	err = VoidAchs( &(vfxapp->Misc) );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = TermAchs( vfxapp->Buff );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = TermAchs( vfxapp->Name );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	return 0;
}

EXTRA VFXREF* SeekVfxAppShader( VFXREF *vfxapp, VFXDEST type )
	{ return SeekNthKid( vfxapp->_node, type ); }

dint debugVfxAppAndShader( void *ud, VFXREF *vfxapp, VFXREF *shader )
{
	ACHS Log = {0};
	COMMON common = {0};
	LOCK *lock = &(common.lock);
	dint err = 0;
	common.ud = ud;
	Log.usual = &common;
	err = TakeLock( lock );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	LockErrors();
	do
	{
		err = InitAchsc( &Log, 1 );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}
		if ( vfxapp )
		{
			LOCK *alock = &(vfxapp->usual->lock);
			err = TakeLock( alock );
			if ( err )
			{
				Log2VfxVaif( XMSG_ERRNO(,err) );
				break;
			}

			SeekVfxRefLog( &Log, vfxapp );
			Log2VfxVaif
				( LINEF "%.*s", LINEV, Log.array, Log.count );

			if ( achsfind( Log.array, "vertex shader" ) )
				shader = SeekVfxAppShader( vfxapp, VFXDEST_VERTICE_BOUND );
			FreeLock( alock );
		}

		if ( shader )
		{
			ACHS *Name = SeekShaderName(shader);
			ACHS *Path = SeekShaderPath(shader,0);
			ACHS *Code = SeekShaderCode(shader);
			LOCK *slock = &(shader->usual->lock);
			err = TakeLock( slock );
			if ( err )
			{
				Log2VfxVaif( XMSG_ERRNO(,err) );
				break;
			}
			SeekVfxRefLog( &Log, shader );
			Log2VfxVaif
			(
LINEF "In '%.*s' shader\n%.*s\n%.*s\n```\n%.*s\n```\n", LINEV,
Name->count,	(achs)(Name->array),
Path->count,	(achs)(Path->array),
Log.count,	(achs)(Log.array),
Code->count,	(achs)(Code->array)
			);
			FreeLock( slock );
		}
	}
	while (0);
	FreeErrors();
	FreeLock( lock );
	TermAchs( &Log );
	return err;
}

void fchar( int c, uint repeat, FILE *file )
	{ for ( ; repeat; fputc( c, file ), --repeat ); }

void pchar( int c, uint repeat ) { fchar( c, repeat, stdout ); }

SHARED_EXP dint makeVfxAppShader
	( void *ud, VFXREF *vfxapp, VFXDEST type, achs name, achs path )
{
	VFXREF *shader = FindShader( NULL, path );
	LOCK *lock = &(vfxapp->usual->lock);
	dint err = 0;

	TakeLock( lock );
	do
	{
		ach *code;
		SOURCE source = {0};
		ACHS *Path = NULL, *Name = NULL, *Code = NULL;

		if ( shader )
		{
			/* shader was already compiled, bind it and continue */
			MarkNthKid( vfxapp->_node, type, shader->_node );
			err = BindShader( vfxapp, shader );
			if ( err )
				Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}

		err = MakeVfxRef( ud, &shader );
		if ( err )
		{
			Log2VfxVaif
( XMSG_ERRNO( "Failed to create shader object\n", err ) );
			break;
		}

		MarkNthKid( vfxapp->_node, type, shader->_node );

		err = makeSrcNamef
		(
			ud, &source, &Name, NULL,
			"#predef __VFXREF_NAME__ %s", name
		);
		if ( err )
		{
			Log2VfxVaif
( XMSG_ERRNO( "Failed name duplication", err ) );
			break;
		}

		err = MakeVoids( ud, &(shader->Misc) );
		if ( err )
		{
			Log2VfxVaif
( XMSG_ERRNO( "Failed setting up path list", err ) );
			break;
		}

		err = makeSrcPath( ud, &source, &Path, NULL, path );
		if ( err )
		{
			Log2VfxVaif
( XMSG_ERRNO( "Failed path duplication", err ) );
			break;
		}

		source.Path = Path;
		err = readIncludes( ud, &source );
		Code = shader->Buff = source.Text;
		code = Code ? Code->array : NULL;
		if ( err || !code )
		{
			err = err ? err : ENODATA;
			Log2VfxVaif
( XMSG_ERRNO( "Failed code preperation, got:\n%s\n", err ), code );
			break;
		}

		MarkVfxRefBound( shader, vfxapp );
		err = InitShader( shader, type );
		if ( err )
		{
			Log2VfxVaif
( XMSG_ERRNO( "Failed to initiate the shader", err ) );
			break;
		}

		err = BindVfxSrc( shader );
		if ( err )
		{
			Log2VfxVaif
( XMSG_ERRNO( "Failed to compile the shader", err ) );
			break;
		}

		err = BindShader( vfxapp, shader );
		if ( err )
		{
			Log2VfxVaif
( XMSG_ERRNO( "Failed to bind the shader", err ) );
			break;
		}

		Log2VfxVaib( "Success\n" );
		shader = NULL;
	}
	while (0);
	if ( err )
		_EndVfxRef( vfxapp->_node );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint InitVfxApp( VFXREF *vfxapp, achs _path, achs _name )
{
	dint err = EINVAL;
	LOCK *lock = &(vfxapp->usual->lock);
	achs names[VFXDEST_SHADERS_COUNT] = {NULL}, name = NULL;
	uint lengs[VFXDEST_SHADERS_COUNT] = {0}, k;

	if ( vfxapp->type )
		return FailVfxRefAlreadyInitialised( vfxapp );

	if ( !_path )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return EINVAL;
	}

	names[VFXDEST_NONE		] = "share";
	names[VFXDEST_COMPUTE_BOUND	] = "other";
	names[VFXDEST_VERTICE_BOUND	] = "point";
	names[VFXDEST_SIMPLEX_BOUND	] = "basic";
	names[VFXDEST_TESCTRL_BOUND	] = "tctrl";
	names[VFXDEST_TESEVAL_BOUND	] = "teval";
	names[VFXDEST_FRAGDYE_BOUND	] = "color";

	for ( k = 0; k < VFXDEST_SHADERS_COUNT; ++k )
		lengs[k] = names[k] ? achsnot0( names[k] ) : 0;

	err = TakeLock( lock );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	do
	{
		SOURCE src = {0};
		WHERE *ins = &(src.ins), *end = &(src.pos);
		ach *text = NULL, *path = NULL, *temp = NULL;
		ACHS *Temp = vfxapp->Buff;
		ACHS *Name = vfxapp->Name;
		ACHS *Path = NULL;
		void *ud = vfxapp->usual->ud;

		vfxapp->type = VFXTYPE_GFXAPP;

		err = MakeAchs( ud, &Path );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			return err;
		}

		vfxapp->Misc = Path;

		err = MarkNumKids( vfxapp->_node, VFXDEST_SHADERS_COUNT );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			return err;
		}

		err = readAllFromPath( _path, Temp );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}

		src.Path = Path;
		src.Text = Temp;
		temp = Temp->array;

		while ( readNextLine( Temp, ins, end ) )
		{
			uint not0 = end->i - ins->i;
			ach *line = temp + ins->i, *init = NULL, *term = NULL;

			if ( achsifind( line, "[vfxapp." ) != line )
				continue;

			init = (ach*)achscharn( line, not0, '.' );
			term = (ach*)achscharn( line, not0, ']' );
			name = init + 1;
			*term = 0;

			if ( _name && achsicmp( name, _name ) )
			{
				name = NULL;
				*term = ']';
				continue;
			}

			Log2VfxVaif( "Creating program '%s'\n", name );
			*term = ']';
			break;
		}

		if ( !name )
		{
			err = ENODATA;
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}

		err = vfxvai.initVfxRefCB[VFXTYPE_GFXAPP]( vfxapp );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}

		while ( readNextLine( Temp, ins, end ) )
		{
			uint not0 = end->i - ins->i, type = 0, leng = 0, used = 0;
			ach *line = temp + ins->i, *init = NULL, *term = NULL;

			if ( *line == '[' )
				break;
			if ( *line == '!' )
				continue;

			init = (ach*)achscharn( line, not0, '=' );
			term = temp + src.pos.i;

			if ( !init )
				continue;

			*term = 0;
			*init = 0;
			text = init + 1;
			leng = (uptr)(init - line);
			used = (uptr)(term - text);
			for ( k = 0; k < VFXDEST_SHADERS_COUNT; ++k )
			{
				if
				(
					achsifindn
					(
						line, leng,
						names[k], lengs[k]
					) != NULL
				)
				{
					type = k;
					break;
				}
			}

			if ( !k )
				goto restore_and_continue;

			if ( k == VFXDEST_SHADERS_COUNT )
			{
				err = EINVAL;
				Log2VfxVaif( XMSG_ERRNO(,err) );
				goto restore_and_return;
			}

			err = InitAchsb( Path, _path );
			if ( err )
			{
				Log2VfxVaif( XMSG_ERRNO(,err) );
				goto restore_and_return;
			}

			path = Path->array;
			dirname( path );
			Path->count = achsnot0( path );
			err = GrowAchsf( Path, "/%.*s", used, text );
			if ( err )
			{
				Log2VfxVaif( XMSG_ERRNO(,err) );
				goto restore_and_return;
			}

			path = Path->array;
			Log2VfxVaif
( "Creating/seeking shader with path '%s'...\n", path );
			err = makeVfxAppShader( ud, vfxapp, type, line, path );
			if ( err )
			{
				Log2VfxVaif
( XMSG_ERRNO( "Failed to create shader for path '%s'\n", err ), path );
				restore_and_return:
				*init = '=';
				*term = ']';
				return err;
			}

			TermAchs( Path );
			restore_and_continue:
			*init = '=';
			*term = ']';
		}

		TermAchs( Path );

		Log2VfxVaib( "Linking program...\n" );
		err = vfxvai.linkVfxAppCB( vfxapp );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			debugVfxAppAndShader( ud, vfxapp, NULL );
		}
		else
		{
			Log2VfxVaib( "Success\n" );
			Log2VfxVaib( "Testing program...\n" );
			err = vfxvai.testVfxAppCB( vfxapp );
			if ( err )
			{
				Log2VfxVaif( XMSG_ERRNO(,err) );
				debugVfxAppAndShader( ud, vfxapp, NULL );
			}
		}
		Log2VfxVaib( "Success\n" );

		err = MarkAchsb( Name, name );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			TermAchs( Temp);
			break;
		}

		TermAchs( Temp );
		err = InitAchsb( Path, _path );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}
	}
	while (0);
	if ( err )
		vfxapp->type = 0;
	FreeLock( lock );
	return err;
}

SHARED_EXP dint BindVfxApp( VFXREF *vfxapp )
{
	LOCK *lock = &(vfxapp->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxvai.bindVfxAppCB( vfxapp );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint BindShader( VFXREF *vfxapp, VFXREF *shader )
{
	LOCK *lock = &(vfxapp->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	do
	{
		MarkVfxRefBound( shader, vfxapp );
		err = vfxvai.bindShaderCB( vfxapp, shader );

		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}

		MarkNthKid( vfxapp->_node, shader->type, shader->_node );
	}
	while (0);
	FreeLock( lock );
	return err;
}

SHARED_EXP dint OustShader( VFXREF *vfxapp, VFXDEST type )
{
	VFXREF *shader = SeekVfxAppShader( vfxapp, type );
	LOCK *lock = &(vfxapp->usual->lock);
	dint err = 0;

	if ( !shader )
		return 0;

	err = TakeLock( lock );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	do
	{
		err = vfxvai.oustShaderCB( vfxapp, shader );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}
		MarkNthKid( vfxapp->_node, type, NULL );
		MarkVfxRefBound( shader, NULL );
	}
	while (0);
	FreeLock( lock );
	return err;
}
SHARED_EXP dint LinkVfxApp( VFXREF *vfxapp )
{
	dint err;
	LOCK *lock = &(vfxapp->usual->lock);
	TakeLock( lock );
	err = vfxvai.linkVfxAppCB( vfxapp );
	FreeLock( lock );
	return err;
}
