#include <extra/viewfx/_/_vfxvai.h>
#include <basic/_/_buffer.h>

SHARED_EXP dint _TERM_VFXTYPE_BUFFER( VFXREF *vfxref )
	{ (void)vfxref; return 0; }

achs vfxbuff_enums[VFXBUFF_COUNT] = {NULL};
achs vfxhint_enums[VFXHINT_COUNT] = {NULL};

SHARED_EXP dint FailVfxBufNotInitialised( VFXREF *vfxbuf )
{
	dint err = ENOTCONN;
	Log2VfxVaif
	(
XMSG_ERRNO( "vfxref (object index %u) was not initialised\n"
"as a buffer!\n", err ), SeekObjectNode( vfxbuf->_node )
	);
	return err;
}

SHARED_EXP achs SeekVfxBuffEnum( VFXBUFF i ) { return vfxbuff_enums[i]; }
SHARED_EXP void InitVfxBuffEnums()
{
#define NAME( I ) vfxbuff_enums[I] = #I
	NAME( VFXBUFF_NONE );
	NAME( VFXBUFF_CALCDISPATCH );
	NAME( VFXBUFF_CMDARG_INPUT );
	NAME( VFXBUFF_INDICE_INPUT );
	NAME( VFXBUFF_PIXELS_INPUT );
	NAME( VFXBUFF_RESULT_ARRAY );
	NAME( VFXBUFF_SHADER_ARRAY );
	NAME( VFXBUFF_SHROUD_INPUT );
	NAME( VFXBUFF_SYMBOL_INPUT );
	NAME( VFXBUFF_WARPER_ARRAY );
#undef NAME
}

SHARED_EXP achs SeekVfxHintEnum( VFXHINT i ) { return vfxhint_enums[i]; }
SHARED_EXP void InitVfxHintEnums()
{
#define NAME( I ) vfxhint_enums[I] = #I
	NAME( VFXHINT_NONE );
	NAME( VFXHINT_DYNAMIC_COPY );
	NAME( VFXHINT_DYNAMIC_DRAW );
	NAME( VFXHINT_DYNAMIC_READ );
	NAME( VFXHINT_FLOWING_COPY );
	NAME( VFXHINT_FLOWING_DRAW );
	NAME( VFXHINT_FLOWING_READ );
	NAME( VFXHINT_LASTING_COPY );
	NAME( VFXHINT_LASTING_DRAW );
	NAME( VFXHINT_LASTING_READ );
#undef NAME
}

SHARED_EXP dint InitVfxBuf( VFXREF *R, VFXBUFF buff, VFXHINT hint )
{
	dint err = 0;
	LOCK *lock = &(R->usual->lock);

	if ( !(R->vfxdef) )
	{
		err = EINVAL;
		Log2VfxVaif
( XMSG_ERRNO("Vfx data type cannot be undefined\n",err) );
		return err;
	}

	err = TakeLock( lock );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	do
	{
		VFXDEF *vfxdef = R->vfxdef;
		BUFFER *buffer = R->Buff;

		R->buff = buff;
		R->hint = hint;

		if ( !(R->bound) || GlobalVfxDest(R->bound->dest) < 1 )
		{
			err = InitBufferc( buffer, vfxdef->bytes, 1 );
			if ( err )
			{
				Log2VfxVaif( XMSG_ERRNO(,err) );
				TermVfxRef( R );
				return err;
			}

			if ( buff )
			{
				err = vfxvai.initVfxBufCB[buff]( R );
				if ( err )
				{
					Log2VfxVaif( XMSG_ERRNO(,err) );
					TermBuffer( buffer );
					break;
				}
			}
		}
	}
	while (0);
	if ( err )
		_EndVfxRef( R->_node );
	FreeLock( lock );
	return err;
}
SHARED_EXP dint OustVfxBuf( VFXREF *vfxbuf )
{
	if ( vfxbuf->buff )
	{
		dint err;
		LOCK *lock = &(vfxbuf->usual->lock);
		TakeLock( lock );
		err = vfxvai.oustVfxBufCB[vfxbuf->buff]( vfxbuf );
		FreeLock( lock );
		return err;
	}
	return FailVfxBufNotInitialised( vfxbuf );
}
SHARED_EXP dint BindVfxBuf( VFXREF *vfxbuf )
{
	if ( vfxbuf->buff )
	{
		dint err;
		LOCK *lock = &(vfxbuf->usual->lock);
		TakeLock( lock );
		err = vfxvai.bindVfxBufCB[vfxbuf->buff]( vfxbuf );
		FreeLock( lock );
		return err;
	}
	return FailVfxBufNotInitialised( vfxbuf );
}
SHARED_EXP dint SendVfxBuf( VFXREF *vfxbuf )
{
	if ( vfxbuf->buff )
	{
		dint err;
		LOCK *lock = &(vfxbuf->usual->lock);
		TakeLock( lock );
		err = vfxvai.sendVfxBufCB[vfxbuf->buff]( vfxbuf );
		FreeLock( lock );
		return err;
	}
	return FailVfxBufNotInitialised( vfxbuf );
}

