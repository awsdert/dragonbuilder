#include <extra/viewfx/_/_vfxvai.h>
#include <basic/_/_buffer.h>

SHARED_EXP dint _TERM_VFXTYPE_CONFIG( VFXREF *vfxref )
	{ return _TERM_VFXTYPE_BUFFER( vfxref ); }

SHARED_EXP dint InitVfxCfg( VFXREF *vfxcfg )
{
	dint err;
	LOCK *lock = &(vfxcfg->usual->lock);
	if ( vfxcfg->type )
		return FailVfxRefAlreadyInitialised( vfxcfg );
	err = TakeLock( lock );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	vfxcfg->type = VFXTYPE_CONFIG;
	err = vfxvai.initVfxRefCB[VFXTYPE_CONFIG]( vfxcfg );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		vfxcfg->type = 0;
	}
	FreeLock( lock );
	return err;
}

SHARED_EXP dint OustVfxCfg( VFXREF *vfxcfg )
{
	LOCK *lock = &(vfxcfg->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxVaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxvai.oustVfxCfgCB( vfxcfg );
	FreeLock( lock );
	return err;
}
SHARED_EXP dint BindVfxCfg( VFXREF *vfxcfg )
{
	dint err;
	LOCK *lock = &(vfxcfg->usual->lock);
	TakeLock( lock );
	err = vfxvai.bindVfxCfgCB( vfxcfg );
	FreeLock( lock );
	return err;
}
SHARED_EXP dint DrawVfxCfg( VFXREF *vfxcfg, VFXDRAW draw, uint occurs )
{
	dint err = 0;
	uint i = 0, symc = 0, *syms = NULL;
	LOCK *lock = &(vfxcfg->usual->lock);

	if ( vfxcfg->type != VFXTYPE_CONFIG )
		return DrawVfxSym( vfxcfg, draw, occurs );

	err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	BindVfxCfg( vfxcfg );

	if ( vfxcfg->buff && vfxcfg->hint < VFXHINT_FLOWING_COPY )
		SendVfxBuf( vfxcfg );

	symc = SeekNumKids( vfxcfg->_node );
	syms = SeekAllKids( vfxcfg->_node );

	for ( i = 0; i < symc; ++i )
	{
		VFXREF *vfxsym = SeekObject( syms[i] );
		if ( !vfxsym )
			continue;
		err = DrawVfxSym( vfxsym, draw, occurs );
		if ( err )
		{
			Log2VfxVaif( XMSG_ERRNO(,err) );
			break;
		}
	}
	FreeLock( lock );
	return err;
}
