#include <extra/viewfx/_/_vfxapi.h>
#include <extra/viewfx/_/_vfxvai.h>

achs vfxtype_enums[VFXTYPE_COUNT] = {NULL};
achs vfxattr_enums[VFXDEST_COUNT] = {NULL};
achs vfxdata_enums[VFXDATA_COUNT] = {NULL};

SHARED_EXP VFXDEF* SeekVfxDef( VFXDATA i ) { return &(vfxvai.vfxdefs[i]); }
SHARED_EXP achs SeekVfxTypeEnum( VFXTYPE i ) { return vfxtype_enums[i]; }
void InitVfxTypeEnums()
{
#define NAME( I ) vfxtype_enums[I] = #I
	NAME( VFXTYPE_NONE );
	NAME( VFXTYPE_GFXAPP );
	NAME( VFXTYPE_SHADER );
	NAME( VFXTYPE_CONFIG );
	NAME( VFXTYPE_SYMBOL );
#undef NAME
}

void InitVfxDestEnums()
{
#define NAME( I ) vfxattr_enums[I] = #I
	NAME( VFXDEST_NONE );
	NAME( VFXDEST_COMPUTE_BOUND );
	NAME( VFXDEST_VERTICE_BOUND );
	NAME( VFXDEST_TESCTRL_BOUND );
	NAME( VFXDEST_TESEVAL_BOUND );
	NAME( VFXDEST_SIMPLEX_BOUND );
	NAME( VFXDEST_FRAGDYE_BOUND );
	NAME( VFXDEST_UNIFORM_VALUE );
	NAME( VFXDEST_UNIFORM_BLOCK );
	NAME( VFXDEST_UNIFORM_ARRAY );
	NAME( VFXDEST_COMPUTE_INDEX );
	NAME( VFXDEST_VERTICE_INDEX );
	NAME( VFXDEST_TESCTRL_INDEX );
	NAME( VFXDEST_TESEVAL_INDEX );
	NAME( VFXDEST_SIMPLEX_INDEX );
	NAME( VFXDEST_FRAGDYE_INDEX );
#undef NAME
}

SHARED_EXP achs SeekVfxDataEnum( VFXDATA i ) { return vfxdata_enums[i]; }
SHARED_EXP void InitVfxDataEnums()
{
#define NAME( I ) vfxdata_enums[I] = #I
	NAME( VFXDATA_NONE );
	NAME( VFXDATA_BOOL );
	NAME( VFXDATA_BVEC2 );
	NAME( VFXDATA_BVEC3 );
	NAME( VFXDATA_BVEC4 );
	NAME( VFXDATA_UCHAR );
	NAME( VFXDATA_USHORT );
	NAME( VFXDATA_UINT );
	NAME( VFXDATA_UVEC2 );
	NAME( VFXDATA_UVEC3 );
	NAME( VFXDATA_UVEC4 );
	NAME( VFXDATA_DCHAR );
	NAME( VFXDATA_DSHORT );
	NAME( VFXDATA_DINT );
	NAME( VFXDATA_IVEC2 );
	NAME( VFXDATA_IVEC3 );
	NAME( VFXDATA_IVEC4 );
	NAME( VFXDATA_FNUM );
	NAME( VFXDATA_FVEC2 );
	NAME( VFXDATA_FVEC3 );
	NAME( VFXDATA_FVEC4 );
	NAME( VFXDATA_MAT2X2 );
	NAME( VFXDATA_MAT2X3 );
	NAME( VFXDATA_MAT2X4 );
	NAME( VFXDATA_MAT3X2 );
	NAME( VFXDATA_MAT3X3 );
	NAME( VFXDATA_MAT3X4 );
	NAME( VFXDATA_MAT4X2 );
	NAME( VFXDATA_MAT4X3 );
	NAME( VFXDATA_MAT4X4 );
	NAME( VFXDATA_DNUM );
	NAME( VFXDATA_DVEC2 );
	NAME( VFXDATA_DVEC3 );
	NAME( VFXDATA_DVEC4 );
#undef NAME
}

void initVfxDef
(
	VFXDATA i,
	ucap Vsize,
	uint count,
	achs named,
	achs basic
)
{
	struct _VFXDEF *D = vfxvai.vfxdefs + i;
	D->type = i;
	D->Vsize = Vsize;
	D->count = count;
	D->named = named;
	D->basic = basic;
	D->bytes = Vsize * count;
	D->libref = vfxvai.seekVfxDefCB(i);
}

void EchoVfxDef( VFXDATA type )
{
	VFXDEF *vfxdef = vfxvai.vfxdefs + type;
	Log2VfxVaif
	(
		"vfxdef: type = %s,\tVsize = %2u, count = %2u, "
		"named = %7s, basic = %s\n",
		SeekVfxDataEnum( vfxdef->type ),
		vfxdef->Vsize, vfxdef->count,
		vfxdef->named, vfxdef->basic
	);
}
void EchoVfxDefs()
{
	VFXDATA i;
	for ( i = 0; i < VFXDATA_COUNT; ++i )
		EchoVfxDef( i );
}

void initVfxDefsArray()
{
#define _VECS( T, I, PFX, MAT ) \
	initVfxDef( (I)+0, sizeof(T), 2, PFX "2", #T MAT "[2]" ); \
	initVfxDef( (I)+1, sizeof(T), 3, PFX "3", #T MAT "[3]"); \
	initVfxDef( (I)+2, sizeof(T), 4, PFX "4", #T MAT "[4]" )

#define VECS( T, I, PFX ) \
	initVfxDef( I, sizeof(T), 1, #T, #T ); \
	_VECS( T, (I)+1, PFX "vec", "" )

#define MATS( T, I, PFX ) \
	_VECS( T, (I)+0, PFX "mat2x", "[2]" ); \
	_VECS( T, (I)+3, PFX "mat3x", "[3]" ); \
	_VECS( T, (I)+6, PFX "mat4x", "[4]" );

	initVfxDef( VFXDATA_NONE, 0, 0, "", "" );
	VECS( bool, VFXDATA_BOOL, "b" );

	initVfxDef( VFXDATA_UCHAR, sizeof(uchar), 1, "uchar", "signed char" );
	initVfxDef( VFXDATA_USHORT, sizeof(ushort), 1, "ushort", "signed short" );
	VECS( uint, VFXDATA_UINT, "u" );

	initVfxDef( VFXDATA_DCHAR, sizeof(dchar), 1, "schar", "signed char" );
	initVfxDef( VFXDATA_DSHORT, sizeof(dshort), 1, "short", "signed short" );
	VECS( int, VFXDATA_DINT, "i" );

	VECS( float, VFXDATA_FNUM, "" );
	MATS( float, VFXDATA_MAT2X2, "" );
	VECS( double, VFXDATA_DNUM, "d" );

#undef MATS
#undef VECS
#undef _VECS

#if 0
	EchoVfxDefs();
#endif
}
