#define BUILD_SHARED_EXTRA
#include <basic/_/_buffer.h>
#include <extra/viewfx/_/_vfxvai.h>

execVfxRef_cb termVfxObjCB[VFXTYPE_COUNT] = {NULL};
SHARED_EXP dint _TERM_VFXTYPE_OTHERS( VFXREF *R ) { (void)R; return 0; }

SHARED_EXP dint FailVfxRefAlreadyInitialised( VFXREF *R )
{
	dint err = EADDRINUSE;
	Log2VfxVaif
	(
		XMSG_ERRNO( "vfxref (object %u) already in use!\n", err ),
		SeekObjectNode(R->_node)
	);
	return err;
}

SHARED_EXP dint	_NewVfxRef( OBJECT *obj )
{
	VFXREF *R = SeekObjectData( obj );
	dint err = 0;
	R->usual = (COMMON*)obj;
	R->_node = obj;

	do
	{
		void *ud = R->usual->ud;
		err = MakeAchsn( ud, &(R->Name), "", 0 );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			break;
		}

		R->h = -1;
		R->w = -1;

		err = MakeBuffer( ud, &(R->Buff) );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			break;
		}

		err = vfxvai.makeVfxRefCB( R );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			break;
		}

		return 0;
	}
	while (0);
	_EndBuffer( obj );
	return err;
}

SHARED_EXP dint _EndVfxRef( OBJECT *obj )
{
	dint err;
	VFXREF *R = SeekObjectData( obj );

	if ( !(R->type) )
		return 0;

	err = vfxvai.termVfxBufCB[R->buff]( R );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	R->vfxdef = NULL;
	R->buff = 0;
	R->hint = 0;
	R->used = 0;
	R->h = -1;
	R->w = -1;

	err = vfxvai.termVfxRefCB[R->type]( R );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	/* We can't declare programs, shaders & symbols safe to use at this
	 * point so just clear the flags that would indicate otherwise */
	R->flags = 0;

	err = termVfxObjCB[R->type]( R );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	R->type = 0;

	err = MarkNumKids( R->_node, 0 );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	err = TermBuffer( R->Buff );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	err = MarkAchsc( R->Name, 0 );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	R->bound = NULL;
	return 0;
}
SHARED_EXP dint	_DelVfxRef( OBJECT *obj )
{
	dint err = 0;
	VFXREF *R = SeekObjectData( obj );
	if ( !(vfxvai.termVfxVaiCB) )
		return 0;
	err = _EndVfxRef( obj );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxvai.voidVfxRefCB( R );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	err = VoidBuffer( &(R->Buff) );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	err = VoidAchs( &(R->Name) );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	R->_node = NULL;
	R->usual = NULL;

	return 0;
}

OBJECT*	_VfxRefObj( void *data ) { return ((VFXREF*)data)->_node; }

SHARED_EXP VFXREF* SeekVfxRef( uint i )
	{ return SeekObjectData( SeekOBJECT( i ) ); }

SHARED_EXP dint MarkVfxRefDataType( VFXREF *R, VFXDATA type )
{
	LOCK *lock = &(R->usual->lock);
	dint err = TakeLock(lock);
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	if ( R->type != VFXTYPE_SYMBOL && !SeekBufferc(R->Buff) )
		R->vfxdef = SeekVfxDef( type );
	else
	{
		err = EADDRINUSE;
		Log2VfxVaif
		(
XMSG_ERRNO( "Cannot override type of '%s' while it is in use as\n"
"either a buffer or symbol!\n", err ), SeekAchsb( R->Name )
		);
	}
	FreeLock( lock );
	return err;
}

SHARED_EXP OBJECT*  SeekVfxRefNode( VFXREF *R ) { return R->_node; }
SHARED_EXP VFXTYPE SeekVfxRefType( VFXREF *R ) { return R->type; }
SHARED_EXP VFXDEST SeekVfxRefDest( VFXREF *R ) { return R->dest; }
SHARED_EXP VFXBUFF SeekVfxRefBuff( VFXREF *R ) { return R->buff; }
SHARED_EXP VFXHINT SeekVfxRefHint( VFXREF *R ) { return R->hint; }
SHARED_EXP VFXREF* SeekVfxRefBound( VFXREF *R ) { return R->bound; }
SHARED_EXP ACHS*   SeekVfxRefNAME( VFXREF *R ) { return R->Name; }
SHARED_EXP achs    SeekVfxRefName( VFXREF *R )
	{ return SeekAchsb( R->Name ); }

SHARED_EXP VFXDEF* SeekVfxDefLink( VFXREF *vfxbuf ) { return vfxbuf->vfxdef; }
SHARED_EXP BUFFER* SeekVfxBufData( VFXREF *R ) { return R->Buff; }
SHARED_EXP ucap SeekVfxBufSize( VFXREF *vfxbuf, dint *h, dint *w )
{
	if ( h ) *h = vfxbuf->h;
	if ( w ) *w = vfxbuf->w;
	return vfxbuf->Buff->bytes;
}

static OBJCLS vfxref_type =
	{ sizeof(VFXREF), VFXREF_DESC, _NewVfxRef, _DelVfxRef };

SHARED_EXP dint VoidVfxRef( VFXREF **R )
	{ return VoidObject( (void**)R, _VfxRefObj ); }
SHARED_EXP dint MakeVfxRef( void *ud, VFXREF **R )
	{ return MakeObject( ud, (void**)R, &vfxref_type ); }

SHARED_EXP dint MakeVfxRefc( void *ud, VFXREF **R, uint not0 )
	{ return MakeVfxRefn( ud, R, NULL, not0 ); }
SHARED_EXP dint MakeVfxReft( void *ud, VFXREF **R, uint size )
{
	dint err = MakeVfxRefn( ud, R, NULL, size );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		VoidVfxRef( R );
		return err;
	}
	MarkAchsc( (*R)->Name, 0 );
	return 0;
}
SHARED_EXP dint MakeVfxRefb( void *ud, VFXREF **R, achs name )
	{ return MakeVfxRefn( ud, R, name, name ? achsnot0(name) : 0 ); }
SHARED_EXP dint MakeVfxRefn( void *ud, VFXREF **R, achs name, uint not0 )
{
	dint err = MakeVfxRef( ud, R );
	do
	{
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			break;
		}
		err = MarkAchsn( (*R)->Name, name, not0 );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			break;
		}
		return 0;
	}
	while (0);
	VoidVfxRef( R );
	return err;
}
SHARED_EXP dint MakeVfxRefv( void *ud, VFXREF **R, achs args, va_list va )
{
	dint err = MakeVfxRef( ud, R );
	do
	{
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			break;
		}
		err = InitAchsv( (*R)->Name, args, va );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			break;
		}
		return 0;
	}
	while (0);
	VoidVfxRef( R );
	return err;
}
SHARED_EXP dint MakeVfxReff( void *ud, VFXREF **R, achs args, ... )
{
	dint err;
	va_list va;
	va_start( va, args );
	err = MakeVfxRefv( ud, R, args, va );
	va_end( va );
	return err;
}

SHARED_EXP dint VoidVfxRefs()
{
	COMMON *Common = SeekALLOBJECTS();
	VFXTYPE i = VFXTYPE_COUNT;
	uint j, count = SeekNumObjects();
	uint leng = achsnot0( VFXREF_DESC );
	LOCK *lock = &(Common->lock);
	OBJECT **objects = NULL;
	void **nodes = NULL;
	VFXREF *R = NULL;
	dint err = 0;
	TakeLock( lock );
	objects = SeekAllObjects();
	nodes = SeekObjVoidBuf();

	for ( j = 0; j < count; ++j )
	{
		OBJECT *object = objects[j];
		achs desc = NULL;
		uint not0 = 0;

		if ( !object )
			continue;

		desc = SeekObjectType( object );
		not0 = achsnot0( desc );
		if ( achsfindn( desc, not0, VFXREF_DESC, leng ) != desc )
			continue;

		nodes[j] = SeekObjectData( object );
	}

	while ( i )
	{
		--i;
		for ( j = count - j; j; --j )
		{
			R = nodes[j];
			if ( !R || R->type != i )
				continue;
			err = VoidVfxRef( &R );
			if ( err )
			{
				Printf( XMSG_ERRNO(,err) );
				goto fail;
			}
			nodes[j] = NULL;
		}
	}

	fail:
	FreeLock( lock );
	return err;
}
SHARED_EXP dint TermVfxRefs()
{
	COMMON *Common = SeekALLOBJECTS();
	uint i, count = SeekNumObjects();
	uint leng = achsnot0( VFXREF_DESC );
	LOCK *lock = &(Common->lock);
	OBJECT **objects = NULL;
	void **nodes = NULL;
	VFXREF *R = NULL;
	VFXTYPE type = 0;
	dint err = 0;
	TakeLock( lock );
	objects = SeekAllObjects();
	nodes = SeekObjVoidBuf();
	for ( i = 0; i < count; ++i )
	{
		OBJECT *object = objects[i];
		achs desc = NULL;
		uint not0 = 0;

		if ( !object )
			continue;

		desc = SeekObjectType( object );
		not0 = achsnot0( desc );
		if ( achsfindn( desc, not0, VFXREF_DESC, leng ) != desc )
			continue;

		nodes[i] = SeekObjectData( object );
	}

	for ( type = VFXTYPE_COUNT; type; )
	{
		--type;
		for ( i = count - 1; i; --i )
		{
			R = nodes[i];
			if ( !R || R->type != type )
				continue;
			err = TermVfxRef( R );
			if ( err )
			{
				Printf( XMSG_ERRNO(,err) );
				goto fail;
			}
		}
	}

	fail:
	FreeLock( lock );
	return err;
}

SHARED_EXP dint MarkVfxRefBound( VFXREF *R, VFXREF *with )
{
	LOCK *lock = &(R->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	R->bound = with;
	FreeLock( lock );
	return 0;
}

SHARED_EXP dint TermVfxRef( VFXREF *R )
{
	LOCK *lock = &(R->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	err = _EndVfxRef( R->_node );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint BindVfxSrc( VFXREF *R )
{
	dint err = 0;
	LOCK *lock = &(R->usual->lock);
	if ( R->type != VFXTYPE_SHADER && R->type != VFXTYPE_SYMBOL )
	{
		err = EINVAL;
		Log2VfxVaif
		(
XMSG_ERRNO( "%s is never expeceted to have a bound source!\n", err ),
SeekVfxTypeEnum( R->type )
		);
		return err;
	}
	if ( R->type == VFXTYPE_SYMBOL && R->buff != VFXBUFF_INDICE_INPUT )
	{
		err = EINVAL;
		Log2VfxVaif
		(
XMSG_ERRNO( "%s is never expeceted to have a bound source!\n", err ),
SeekVfxBuffEnum( R->buff )
		);
		return err;
	}
	err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxvai.bindVfxSrcCB[R->type]( R );
	FreeLock( lock );
	return err;
}
