#define BUILD_SHARED_EXTRA
#include <extra/viewfx/_/_vfxvai.h>
#include <basic/_/_buffer.h>

SHARED_EXP dint _TERM_VFXTYPE_SYMBOL( VFXREF *vfxref )
	{ return _TERM_VFXTYPE_BUFFER( vfxref ); }

SHARED_EXP dint FailVfxSymNotInitialised( VFXREF *vfxsym )
{
	dint err = ENOTCONN;
	Printf( XMSG_ERRNO(,err) );
	Printf
	(
		"vfxref (object index %u) was not initialised a symbol, call "
		"FindVfxSym() first!\n", SeekObjectNode( vfxsym->_node )
	);
	return err;
}

SHARED_EXP dint MarkVfxSymTakes( VFXREF *vfxsym, uint num )
{
	LOCK *lock = &(vfxsym->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	vfxsym->takes = num;
	FreeLock( lock );
	return 0;
}

SHARED_EXP dint MarkVfxSym( VFXREF *vfxsym )
{
	LOCK *lock = &(vfxsym->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxvai.markVfxSymCB[vfxsym->dest]( vfxsym );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint SendVfxSym( VFXREF *vfxsym )
{
	LOCK *lock = &(vfxsym->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxvai.sendVfxSymCB[vfxsym->dest]( vfxsym );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint TakeVfxSym( VFXREF *vfxsym )
{
	LOCK *lock = &(vfxsym->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	DEBUG_EXEC
	(
		Log2VfxVaif
		(
			">\tEnabling VfxSym '%s'\n",
			SeekAchsb(vfxsym->Name)
		);
	);
	err = vfxvai.takeVfxSymCB[vfxsym->dest]( vfxsym );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint DenyVfxSym( VFXREF *vfxsym )
{
	LOCK *lock = &(vfxsym->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	DEBUG_EXEC
	(
		Log2VfxVaif
		(
			">\tDisabling VfxSym '%s'\n",
			SeekAchsb(vfxsym->Name)
		);
	);
	err = vfxvai.denyVfxSymCB[vfxsym->dest]( vfxsym );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint LoseVfxSym( VFXREF *vfxsym )
{
	LOCK *lock = &(vfxsym->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxvai.loseVfxSymCB[vfxsym->dest]( vfxsym );
	vfxsym->dest = 0;
	vfxsym->type = 0;
	FreeLock( lock );
	return err;
}
SHARED_EXP dint GlobalVfxDest( VFXDEST dest )
{
	if ( !dest || dest == VFXDEST_COUNT )
		return -1;
	return (dest >= VFXDEST_UNIFORM_FIRST && dest <= VFXDEST_UNIFORM_FINAL);
}
SHARED_EXP dint FindVfxSym( VFXREF *vfxapp, VFXREF *vfxsym, VFXDEST dest )
{
	dint err = EINVAL, ret = 0;
	LOCK *lock = &(vfxsym->usual->lock);
	VFXREF *parent = SeekParent( vfxsym->_node );
	VFXREF *bound = vfxsym->bound;
	achs name = NULL;
	VFXBUFF buff = bound ? bound->buff : 0;

	if ( buff == VFXBUFF_INDICE_INPUT ) buff = 0;

	if ( vfxsym->type )
		return FailVfxRefAlreadyInitialised( vfxsym );

	name = SeekAchsb( vfxsym->Name );
	if ( !name || !name[0] )
	{
		Printf( XMSG_ERRNO(,err) );
		Puts( "You must name the vfxsym before trying to find it!\n" );
		return err;
	}

	if ( !parent )
	{
		Printf( XMSG_ERRNO(,err) );
		Puts
		(
"All vfxsyms must have parents! This is so that the branches that check for\n"
"a parent can be skipped. No extra memory is take for this.\n"
		);
		return err;
	}

	ret = GlobalVfxDest( dest );
	if ( ret < 0 )
	{
		Printf( XMSG_ERRNO(,err) );
		Puts
		(
			"VFXDEST_NONE & VFXDEST_COUNT are invalid destination "
			"types to give a vfxsym!\n"
		);
		return err;
	}
	else if ( !ret )
	{
		if ( !buff )
		{
			Printf( XMSG_ERRNO(,err) );
			Puts( "vfxsyms can only be bound to vfxbufs.\n" );
			return err;
		}
	}
	else if ( bound && buff && buff != VFXBUFF_GLOBAL_INPUT )
	{
		Printf( XMSG_ERRNO(,err) );
		Puts( "Only bind uniforms to NULL or uniform buffers!\n" );
		return err;
	}

	if
	(
		(
			dest >= VFXDEST_COMPUTE_INDEX
			&& vfxsym->buff != VFXBUFF_INDICE_INPUT
		)
		||
		(
			dest < VFXDEST_COMPUTE_INDEX
			&& vfxsym->buff == VFXBUFF_INDICE_INPUT
		)
	)
	{
		Printf( XMSG_ERRNO(,err) );
		Puts( "Indexed vfxsyms initialised as index buffer first!\n" );
		return err;
	}

	err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	/* Clear the flag that indicates we found the symbol as we may be
	 * searching a different shader &/or graphics program */
	vfxsym->flags &= ~O_VFXREF_VALID;
	vfxsym->type = VFXTYPE_SYMBOL;
	vfxsym->dest = dest;

	if ( parent->dest )
	{
		uint *kids = SeekAllKids( parent->_node );
		uint i, count = SeekNumKids( parent->_node );

		vfxsym->offset = parent->offset;
		for ( i = 0; i < count; ++i )
		{
			OBJECT *object = SeekOBJECT( kids[i] );
			VFXREF *child = SeekObjectData( object );
			if ( object == vfxsym->_node )
				break;
			vfxsym->offset += child->vfxdef->Vsize * child->takes;
		}
	}

	err = vfxvai.findVfxSymCB[dest]( vfxapp, vfxsym );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		vfxsym->dest = 0;
		vfxsym->type = 0;
		vfxsym->flags ^= O_VFXREF_VALID;
	}

	FreeLock( lock );
	return err;
}

SHARED_EXP dint DrawVfxSym( VFXREF *vfxsym, VFXDRAW draw, uint occurs )
{
	dint err = 0;
	uint i = 0, *syms = NULL, symc = 0;
	LOCK *lock = &(vfxsym->usual->lock);
	if ( !(vfxsym->dest) )
		return FailVfxSymNotInitialised( vfxsym );
	err = TakeLock( lock );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	TakeVfxSym( vfxsym );
	if ( GlobalVfxDest( vfxsym->dest ) )
		SendVfxSym( vfxsym );
	else if ( vfxsym->buff == VFXBUFF_INDICE_INPUT )
		goto done;
	symc = SeekNumKids( vfxsym->_node );
	syms = SeekAllKids( vfxsym->_node );
	for ( i = 0; i < symc; ++i )
	{
		VFXREF *vfxref = SeekObject( syms[i] );
		if ( !vfxref )
			continue;
		err = DrawVfxSym( vfxref, draw, occurs );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			break;
		}
	}
	done:
	if ( !err )
	{
		if ( !(vfxsym->buff) )
			MarkVfxSym( vfxsym );
		else
		{
			if ( vfxsym->buff == VFXBUFF_INDICE_INPUT )
				BindVfxBuf( vfxsym->bound );
			BindVfxBuf( vfxsym );
		}
		err = vfxvai.drawVfxSymCB[vfxsym->dest]( vfxsym, draw, occurs );
	}
	DenyVfxSym( vfxsym );
	FreeLock( lock );
	return err;
}

#define CODE( PFX, I, VEC ) \
{ \
	dint err; \
	LOCK *lock = &(vfxsym->usual->lock); \
	VFXDATA type = vfxsym->vfxdef->type; \
	if \
	( \
		!!(vfxsym->bound) || !(vfxsym->used) || type != (I) \
		|| !(vfxsym->flags & O_VFXREF_VALID) \
	) \
		return EINVAL; \
	err = TakeLock( lock ); \
	if ( err ) \
	{ \
		Printf( XMSG_ERRNO(,err) ); \
		return err; \
	} \
	err = vfxvai.sendGlobalCB[I]( vfxsym, VEC ); \
	FreeLock( lock ); \
	return err; \
}

#define NAME( SFX, VAR ) SHARED_EXP dint SendVfxSym##SFX( VFXREF *vfxsym, VAR )
#define FUNC( T, I, PFX ) \
NAME( PFX##1, T val	) CODE( PFX, I+0, &val	) \
NAME( PFX##2, T vec[2]	) CODE( PFX, I+1, vec	) \
NAME( PFX##3, T vec[3]	) CODE( PFX, I+2, vec	) \
NAME( PFX##4, T vec[4]	) CODE( PFX, I+3, vec	)

FUNC( uint, VFXDATA_UINT, u )
FUNC( dint, VFXDATA_DINT, i )
FUNC( fnum, VFXDATA_FNUM, f )
FUNC( dnum, VFXDATA_DNUM, d )

#undef FUNC
#undef NAME
#undef CODE
