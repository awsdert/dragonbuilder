#include <extra/viewfx/_/_vfxwin.h>
VFXWAI	vfxwai = {0};
ACHS	vfxwai_log = {0};
COMMON	vfxwai_usual = {0};

dint _NewVfxWin( OBJECT *obj )
{
	VFXWIN *W = SeekObjectData( obj );
	W->usual = (COMMON*)obj;
	W->_node = obj;
	return vfxwai.makeVfxWinCB( W );
}
dint _EndVfxWin( OBJECT *obj )
{
	VFXWIN *W = SeekObjectData( obj );
	dint err = KillVfxWin( W );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	return vfxwai.shutVfxWinCB( W );
}
dint _DelVfxWin( OBJECT *obj )
{
	VFXWIN *W = SeekObjectData( obj );
	dint err = ShutVfxWin( W );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	return vfxwai.voidVfxWinCB( W );
}

OBJECT*	_VfxWinObj( void *data ) { return ((VFXWIN*)data)->_node; }

SHARED_EXP dint	MarkVfxWinUD( VFXWIN *vfxwin, void *ud )
{
	LOCK *lock = &(vfxwin->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	vfxwin->ud = ud;
	FreeLock( lock );
	return 0;
}
SHARED_EXP dint	MarkVfxWinPos( VFXWIN *vfxwin, dint x, dint y )
{
	LOCK *lock = &(vfxwin->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxwai.markVfxWinPosCB( vfxwin, x, y );
	FreeLock( lock );
	return err;
}
SHARED_EXP dint	MarkVfxWinSize( VFXWIN *vfxwin, dint h, dint w )
{
	LOCK *lock = &(vfxwin->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxwai.markVfxWinSizeCB( vfxwin, h, w );
	FreeLock( lock );
	return err;
}
SHARED_EXP void* SeekVfxWinUD( VFXWIN *vfxwin ) { return vfxwin->ud; }
SHARED_EXP dint	SeekVfxWinPos( VFXWIN *vfxwin, dint *x, dint *y )
	{ return vfxwai.seekVfxWinPosCB( vfxwin, x, y ); }
SHARED_EXP dint	SeekVfxWinSize( VFXWIN *vfxwin, dint *h, dint *w )
	{ return vfxwai.seekVfxWinSizeCB( vfxwin, h, w ); }

SHARED_EXP dint InitVfxWai( void *ud, MODULE *module, achs args[] )
{
	dint err = 0;
	ACHS *Log = &(vfxwai_log);
	COMMON *usual = &(vfxwai_usual);
	LOCK *lock = &(usual->lock);
	vfxwai.usual = usual;
	Log->usual = usual;
	TakeLock( lock );
	do
	{
		initVfxWai_cb init = NULL;
		if ( usual->ud )
		{
			err = EADDRINUSE;
			break;
		}
		usual->ud = ud;
		Puts( "Initialising window api wrapper...\n" );
		init = (initVfxWai_cb)FindSymbol( module, "libInitVfxWai" );
		err = init ? init( &vfxwai, args ) : EADDRNOTAVAIL;
		if ( err )
		{
			Printf
( XMSG_ERRNO( "Couldn't initialise wrapped api!\n", err ) );
			break;
		}
		Puts( "Initialised window api.\n" );
	}
	while (0);
	FreeLock( lock );
	return err;
}

SHARED_EXP dint SeekVfxWinLog( ACHS *dst, VFXWIN *vfxwin )
{
	LOCK *lock = vfxwin ? NULL : &(vfxwai_usual.lock);
	ACHS *Log = vfxwin ? NULL : &(vfxwai_log);
	dint err = 0;
	if ( vfxwin )
		return ENOTSUP;
	err = TakeLock( lock );
	if ( err )
		return err;
	err = MarkAchsn( dst, Log->array, Log->count );
	Log->count = 0;
	FreeLock( lock );
	return err;
}

SHARED_EXP dint TermVfxWaiLog()
{
	if ( vfxwai.usual )
		return EADDRINUSE;
	return TermAchs( &(vfxwai_log) );
}

SHARED_EXP dint TermVfxWai( MODULE *module )
{
	dint err = 0;
	uint i, count = SeekNumObjects();
	uint leng = achsnot0( VFXWIN_DESC );
	OBJECT **objects = SeekAllObjects();
	(void)module;
	if ( !(vfxwai.usual) )
		return 0;
	LockErrors();
	for ( i = 0; i < count; ++i )
	{
		OBJECT *object = objects[i];
		achs desc = NULL;
		uint not0 = 0;

		if ( !object )
			continue;

		desc = SeekObjectType( object );
		not0 = achsnot0( desc );

		if ( achsfindn( desc, not0, VFXWIN_DESC, leng ) != desc )
			continue;

		err = VoidOBJECT( &object );
		if ( err )
		{
			Log2VfxWaif( XMSG_ERRNO(,err) );
			break;
		}
	}
	while (0);
	memset( &vfxwai, 0, sizeof(VFXWAI) );
	FreeErrors();
	return err;
}

SHARED_EXP dint PollVfxWai() { return vfxwai.pollVfxWaiCB(); }

SHARED_EXP dint DeadVfxWin( VFXWIN *vfxwin )
{
	LOCK *lock = &(vfxwin->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxwai.deadVfxWinCB( vfxwin );
	FreeLock( lock );
	return err;
}
SHARED_EXP dint BindVfxWin( VFXWIN *vfxwin )
{
	LOCK *lock = &(vfxwin->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxwai.bindVfxWinCB( vfxwin );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint OpenVfxWin( VFXWIN *vfxwin, achs title, dint h, dint w )
{
	LOCK *lock = &(vfxwin->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxwai.openVfxWinCB( vfxwin, title, h, w );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint SwapVfxWinBufs( VFXWIN *vfxwin )
{
	LOCK *lock = &(vfxwin->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxwai.swapVfxWinBufsCB( vfxwin );
	FreeLock( lock );
	return err;
}

SHARED_EXP dint KillVfxWin( VFXWIN *vfxwin )
{
	LOCK *lock = &(vfxwin->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = vfxwai.killVfxWinCB( vfxwin );
	FreeLock( lock );
	return err;
}


SHARED_EXP bool BusyVfxKey( VFXWIN *vfxwin, VFXKEY key )
	{ return vfxwai.busyVfxKeyCB( vfxwin, key ); }

SHARED_EXP dint ShutVfxWin( VFXWIN *W )
{
	LOCK *lock = &(W->usual->lock);
	dint err = TakeLock( lock );
	if ( err )
	{
		Log2VfxWaif( XMSG_ERRNO(,err) );
		return err;
	}
	err = _EndVfxWin( W->_node );
	FreeLock( lock );
	return err;
}

static OBJCLS vfxwin_type =
	{ sizeof(VFXWIN), VFXWIN_DESC, _NewVfxWin, _DelVfxWin };

SHARED_EXP dint VoidVfxWin( VFXWIN **W )
	{ return VoidObject( (void**)W, _VfxWinObj ); }
SHARED_EXP dint MakeVfxWin( void *ud, VFXWIN **W )
	{ return MakeObject( ud, (void**)W, &vfxwin_type ); }

SHARED_EXP dint Log2VfxWaib( achs text )
	{ return GrowAchsb( &(vfxwai_log), text ); }
SHARED_EXP dint Log2VfxWain( achs text, uint not0 )
	{ return GrowAchsn( &(vfxwai_log), text, not0 ); }
SHARED_EXP dint Log2VfxWaiv( achs args, va_list va )
	{ return GrowAchsv( &(vfxwai_log), args, va ); }
SHARED_EXP dint Log2VfxWaif( achs args, ... )
{
	dint err;
	va_list va;
	va_start( va, args );
	err = Log2VfxWaiv( args, va );
	va_end( va );
	return err;
}

#define FUNC( T, ACTION ) \
SHARED_EXP dint BindVfxWinOn##ACTION( VFXWIN *vfxwin, T on##ACTION ) \
{ \
	LOCK *lock = &(vfxwin->usual->lock); \
	dint err = TakeLock( lock ); \
	if ( err ) \
	{ \
		Log2VfxWaif( XMSG_ERRNO(,err) ); \
		return err; \
	} \
	do \
	{ \
		err = vfxwai.bindVfxWinOn##ACTION##CB( vfxwin ); \
		if ( err ) \
		{ \
			Log2VfxWaif( XMSG_ERRNO(,err) ); \
			break; \
		} \
		vfxwin->on##ACTION = on##ACTION; \
	} \
	while (0); \
	FreeLock( lock ); \
	return err; \
}\
SHARED_EXP dint OustVfxWinOn##ACTION( VFXWIN *vfxwin ) \
{ \
	dint err = 0; \
	LOCK *lock = &(vfxwin->usual->lock); \
	TakeLock( lock ); \
	do \
	{ \
		err = vfxwai.oustVfxWinOn##ACTION##CB( vfxwin ); \
		if ( err ) \
		{ \
			Log2VfxWaif( XMSG_ERRNO(,err) ); \
			break; \
		} \
		vfxwin->on##ACTION = NULL; \
	} \
	while (0); \
	FreeLock( lock ); \
	return err; \
}

FUNC( onVfxWinAct_cb, Die )
FUNC( onVfxWinAct_cb, Dye )
FUNC( onVfxWinSet_cb, Aim )
FUNC( onVfxWinSet_cb, See )
FUNC( onVfxWinSet_cb, Min )
FUNC( onVfxWinDuo_cb, Mov )
FUNC( onVfxWinDuo_cb, Alt )
FUNC( onVfxWinDpi_cb, Dpi )
FUNC( onVfxWinKey_cb, Key )

#undef FUNC
