#include <basic/stream.h>
#include <basic/_/_buffer.h>
#include <extra/zipped/_/_zlib.h>
const uchar zlib_map_cfgs[32] =
{
	16, 17, 18,  0,  8,  7,
	 9,  6, 10,  5, 11,  4,
	12,  3, 13,  2, 14,  1,
	15, 19, 31, 31, 31, 31
};

const ZLIB_IMPLIED zlib_implied_cfg_data =
{
	/* get extra bits */
	{
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		2, 3, 7, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0
	},
	/* base cpy value */
	{
		1, 1,  1, 1, 1, 1, 1, 1,
        1, 1,  1, 1, 1, 1, 1, 1,
		3, 3, 11, 0, 0, 0, 0, 0,
		0, 0,  0, 0, 0, 0, 0, 0
	}
};

const ZLIB_IMPLIED zlib_implied_lit_data =
{
	/* get extra bits */
	{
		0, 0, 0, 0, 0, 0, 0, 0,
		1, 1, 1, 1, 2, 2, 2, 2,
		3, 3, 3, 3,	4, 4, 4, 4,
		5, 5, 5, 5, 0, 0, 0, 0
	},
	/* base cpy value */
	{
		  3,   4,   5,   6,   7,   8,   9,  10,
		 11,  13,  15,  17,  19,  23,  27,  31,
		 35,  43,  51,  59,  67,  83,  99, 115,
		131, 163, 195, 227, 258,   0,   0,   0,
	}
};

const ZLIB_IMPLIED zlib_implied_cpy_data =
{
	/* get extra bits */
	{
		 0,  0,  0,  0,  1,  1,  2,  2,
		 3,  3,  4,  4,  5,  5,  6,  6,
		 7,  7,  8,  8,  9,  9, 10, 10,
		11, 11, 12, 12, 13, 13,	 0,  0
	},
	/* base cpy value */
	{
			 1,    2,    3,     4,     5,     7,    9,    13,
			17,   25,   33,    49,    65,    97,  129,   193,
		   257,  385,  513,   769,  1025,  1537, 2049,  3073,
		  4097, 6145, 8193, 12289, 16385, 24577,    0,     0
	}
};


void WrZlibBids( ZLIB_SYMS *Syms )
{
	uint i, bid = 0, len;
	ZLIB_SYM *syms = Syms->syms, *sym;

	for ( len = 1; len <= Syms->lcap; ++len )
	{
		for ( i = 0; i < Syms->used; ++i )
		{
			sym = syms + i;

			if ( sym->len != len )
				continue;

			Syms->hcap = bid;
			sym->bid = bid++;
		}

		bid <<= 1;
	}
}

dint RdZlibBits( ZLIB *zlib, u32 *dst, u32 num, bool keep_final_pos )
{
	u32 n, bits = 0;
	ZLIBIO *io = zlib->rd;
	ucap b = io->bit % (BUFSIZ * CHAR_BIT);
	uchar bit = 1u << (b % CHAR_BIT);
	ucap B = b / CHAR_BIT;
	bool read = false;
	dint err;

	if ( num > bitsof(u32) )
	{
		err = EINVAL;
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	b = io->bit;
	for ( n = 0; n < num; ++n )
	{
		bits <<= 1;
		bits |= !!(io->buff[B] & bit);

		b++;
		bit <<= 1;
		if ( !bit )
		{
			B++;
			bit = 1u;
		}

		if ( B == BUFSIZ )
		{
			dint err;
			memset( io->buff, 0, BUFSIZ );
			err = io->next( io, BUFSIZ );
			if ( err )
			{
				Printf( XMSG_ERRNO(,err) );
				*dst = bits;
				return err;
			}
			B = 0;
			bit = 1u;
			read = true;
		}
	}

	*dst = bits;
	if ( keep_final_pos )
		io->bit = b;
	else if ( read )
		io->prev( io, BUFSIZ );
	return 0;
}

dint RdZlibNext( ZLIB *zlib, ZLIB_SYMS *Syms, ZLIB_SYM **Sym )
{
	ZLIB_SYM *syms = Syms->syms, *sym;
	u32 len, bid = 0;
	uint i;
	dint err;

	*Sym = NULL;
	for ( len = 1; len <= Syms->lcap; ++len )
	{
		err = RdZlibBits( zlib, &bid, len, false );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			return err;
		}

		/*FlipBits( &bid, len );*/
		for ( i = 0; i < Syms->used; ++i )
		{
			sym = syms + i;
			if ( !(sym->use) || sym->len != len )
				continue;
			if ( sym->bid == bid )
			{
				RdZlibBits( zlib, &bid, len, true );
				return 0;
			}
		}
	}

	return -1;
}

dint RdZlibData( ZLIB *zlib );

dint RdZlibCfgs( ZLIB *zlib )
{
	ZLIB_SYM_TYPES id = ZLIB_SYM_TYPE_CFG;
	ZLIB_SYMS *Cfgs = &(zlib->syms[id]);
	ZLIB_SYM *cfgs = Cfgs->syms, *cfg;
	uint i, pos;
	dint err;

	for ( i = 0; i < Cfgs->bids; ++i )
	{
		pos = (i < 20) ? zlib_map_cfgs[i] : 31;
		cfg = cfgs + pos;

		err = RdZlibBits( zlib, &(cfg->len), 3, true );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			return err;
		}

		cfg->pos = i;
		cfg->val = pos;
		cfg->use = !!(cfg->len);
		if ( cfg->len > Cfgs->lcap )
			Cfgs->lcap = cfg->len;
	}

	Cfgs->used = 19;
	WrZlibBids( Cfgs );

	return 0;
}

dint RdZlibSyms( ZLIB *zlib, ZLIB_SYMS *Syms, u32 start_values )
{
	ZLIB_SYMS *Cfgs = &(zlib->syms[ZLIB_SYM_TYPE_CFG]);
	ZLIB_SYM *syms = Syms->syms, *Sym, *Cfg, Temp;
	u32 i, cpy, get, pos = 0, prv = 0;
	dint err;

	while ( pos < Syms->bids )
	{
		err = RdZlibNext( zlib, Cfgs, &Cfg );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			return err;
		}

		cpy = 1;
		get = zlib_implied_cfg_data.get[Cfg->val];
		memset( &Temp, 0, sizeof(ZLIB_SYM) );

		if ( Cfg->val < 16 )
			Temp.len = Cfg->val;
		else
		{
			RdZlibBits( zlib, &cpy, get, true );
			cpy += zlib_implied_cfg_data.cpy[Cfg->val];
			Temp.len = (Cfg->val == 16) ? prv : syms->len;

			if ( !Temp.len )
			{
				pos += cpy;
				cpy = 0;
			}
		}

		Temp.use = !!(Temp.len);
		prv = Temp.len;

		if ( Temp.len > Syms->lcap )
			Syms->lcap = Temp.len;

		for ( i = 0; i < cpy; ++i, ++pos )
		{
			Sym = syms + pos;

			Sym->val = start_values + pos;
			Sym->use = Temp.use;
			Sym->len = Temp.len;
			Sym->pos = pos;
		}
	}

	Syms->used = pos;
	WrZlibBids( Syms );

	return 0;
}


/* TODO: Implement these */
dint RdZlibIo( ZLIBIO *rd, ZLIBIO *wr ) { (void)rd; (void)wr; return ENOTSUP; }
dint WrZlibIo( ZLIBIO *rd, ZLIBIO *wr ) { (void)rd; (void)wr; return ENOTSUP; }

/* Modified form of what's provided here:
 * https://github.com/libyal/assorted/blob/main/documentation/Deflate%20(zlib)%20compressed%20data%20format.asciidoc
 */
ulong adler32_fetch( uch *buff, size_t size, ulong prior_key )
{
	size_t i, j;
	uint32_t lower_word    = prior_key & 0xffff;
	uint32_t upper_word    = (prior_key >> 16) & 0xffff;

	lower_word += buff[0];
	upper_word += lower_word;
	--size;

	for( i = j = 1; i < size; ++i, ++j )
	{
		lower_word += buff[i];
		upper_word += lower_word;

		if ( j == 0x15b0 )
		{
			lower_word %= 0xfff1;
			upper_word %= 0xfff1;
			j = 0;
		}
	}

	lower_word += buff[i];
	upper_word += lower_word;

	lower_word %= 0xfff1;
	upper_word %= 0xfff1;

	return( ( upper_word << 16 ) | lower_word );
}

#if 0
int ExpandZlibArchive( ZLIB *zlib, STREAM *dst, STREAM *src )
{
	uch *into;
	dint err;
	intmax_t expect, actual;
	uint method, xflags, check;

	method = IStreamBits( &Stream, 8, true );
	xflags = IStreamBits( &Stream, 8, true );
	check = ((xflags >> 3) & 0x1F);

	zlib->IntoID = IntoID;
	zlib->method = method;
	zlib->xflags = xflags;
	zlib->Stream = src;

	err = ExpandZlibIStream( zlib, dst, src );

	if ( err )
		Printf( XMSG_ERRNO(,err) );

	expect = SeekStreamBits( src, 16, true );
	large_endian_to_local_endian( &expect, 2 );
	expect += ((check & 0xF) << 8);
	expect -= ((check & 0x10) << 8);
	actual = adler32_fetch( into + used, Into->used - used, 1 );

	return err;
}
#endif
