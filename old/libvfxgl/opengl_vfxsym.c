#include "opengl_api.h"

dint libInitVfxSym( VFXREF *vfxsym ) { (void)vfxsym; return 0; }
dint libTermVfxSym( VFXREF *vfxsym ) { (void)vfxsym; return 0; }
dint libSendVfxSymBuffer( VFXREF *vfxsym )
{
	libBindVfxBuf( vfxsym );
	libSendVfxBuf( vfxsym );
	return 0;
}
dint libLoseVfxSym( VFXREF *vfxsym )
{
	LIB_VFXREF *sym = vfxsym->libref;
	sym->ref = (GLuint)-1;
	return 0;
}

dint libFindVerticeBoundVfxSym( VFXREF *vfxapp, VFXREF *vfxsym )
{
	LIB_VFXREF *app = vfxapp->libref;
	LIB_VFXREF *sym = vfxsym->libref;
	achs name = SeekAchsb( vfxsym->Name );

	Log2VfxVaif
( LINEF "glGetAttribLocation( %lu, '%s' );\n", LINEV, app->ref, name );
	sym->ref = (GLint)glGetAttribLocation( app->ref, name );

	vfxsym->used = (sym->ref >= 0);
	if ( !(vfxsym->used) )
	{
		dint err = ENOTRECOVERABLE;
		Log2VfxVaif
( XMSG_ERRNO( "Was given location %ld\n", err ), sym->ref );
		return err;
	}

	return 0;
}

dint libFindUniformArrayVfxSym( VFXREF *vfxapp, VFXREF *vfxsym )
{
	VFXREF *vfxtop = SeekParent( vfxsym->_node );
	LIB_VFXREF *top = vfxtop->libref;
	LIB_VFXREF *app = vfxapp->libref;
	LIB_VFXREF *sym = vfxsym->libref;
	achs name = SeekAchsb( vfxsym->Name );

	if ( vfxsym->buff )
	{
		Log2VfxVaif
		(
			LINEF "glGetUniformBlockIndex( %lu, '%s' );\n",
			LINEV, app->ref, name
		);
		sym->ref = (GLint)glGetUniformBlockIndex( app->ref, name );
	}
	else
	{
		VFXREF *vfxprv = SeekPrvSib( vfxsym->_node );
		LIB_VFXREF *prv = vfxprv ? vfxprv->libref : NULL;
		if ( prv )
		{
			Log2VfxVaif
( LINEF "sym->ref = prv->ref /* %ld */ + 1;\n", LINEV, prv->ref );
			sym->ref = prv->ref + 1;
		}
		else
		{
			Log2VfxVaif
( LINEF "sym->ref = top->ref /* %ld */;\n", LINEV, top->ref );
			sym->ref = top->ref;
		}
	}

	vfxsym->used = (sym->ref >= 0);
	if ( !(vfxsym->used) )
	{
		dint err = ENOTRECOVERABLE;
		Printf( XMSG_ERRNO(,err) );
		Printf( "Was given location %ld\n", sym->ref );
		return err;
	}

	return 0;
}

dint libFindUniformBlockVfxSym( VFXREF *vfxapp, VFXREF *vfxsym )
{
	VFXREF *vfxtop = SeekParent( vfxsym->_node );
	LIB_VFXREF *top = vfxtop->libref;
	LIB_VFXREF *app = vfxapp->libref;
	LIB_VFXREF *sym = vfxsym->libref;
	achs name = SeekAchsb( vfxsym->Name );

	if ( vfxtop->dest == VFXDEST_UNIFORM_BLOCK )
	{
		VFXREF *vfxprv = SeekPrvSib( vfxsym->_node );
		LIB_VFXREF *prv = NULL;
		if ( !vfxprv )
		{
			Log2VfxVaif
( LINEF "sym->ref = top->ref /* %ld */;\n", LINEV, top->ref );
			sym->ref = top->ref + 1;
		}
		else
		{
			uint count = 1;
			while ( vfxprv->dest == VFXDEST_UNIFORM_BLOCK )
				vfxprv = SeekEndKid( vfxprv->_node );
			prv = vfxprv->libref;
			if ( vfxprv->dest == VFXDEST_UNIFORM_ARRAY )
				count = vfxprv->takes;
			Log2VfxVaif
			(
LINEF "sym->ref = prv->ref /* %ld */ + count /* %u */;\n",
LINEV, prv->ref, count
			);
			sym->ref = prv->ref + count;
		}
	}
	else
	{
		Log2VfxVaif
		(
			LINEF "glGetUniformBlockIndex( %lu, '%s' );\n",
			LINEV, app->ref, name
		);
		sym->ref = (GLint)glGetUniformBlockIndex( app->ref, name );
	}

	vfxsym->used = (sym->ref >= 0);
	if ( !(vfxsym->used) )
	{
		dint err = ENOTRECOVERABLE;
		Printf( XMSG_ERRNO(,err) );
		Printf( "Was given location %ld\n", sym->ref );
		return err;
	}

	return 0;
}

dint libFindUniformValueVfxSym( VFXREF *vfxapp, VFXREF *vfxsym )
{
	VFXREF *vfxtop = SeekParent( vfxsym->_node );
	LIB_VFXREF *top = vfxtop->libref;
	LIB_VFXREF *app = vfxapp->libref;
	LIB_VFXREF *sym = vfxsym->libref;
	achs name = SeekAchsb( vfxsym->Name );

	if ( vfxtop->dest == VFXDEST_UNIFORM_BLOCK )
	{
		VFXREF *vfxprv = SeekPrvSib( vfxsym->_node );
		LIB_VFXREF *prv = vfxprv ? vfxprv->libref : NULL;
		if ( prv )
		{
			Log2VfxVaif
			(
				LINEF "sym->ref = prv->ref /* %u */ + 1;\n",
				LINEV, prv->ref
			);
			sym->ref = prv->ref + 1;
		}
		else
		{
			Log2VfxVaif
			(
				LINEF "sym->ref = top->ref /* %u */;\n",
				LINEV, top->ref
			);
			sym->ref = top->ref;
		}
	}
	else
	{
		Log2VfxVaif
		(
			LINEF "glGetUniformLocation( %lu, '%s' );\n",
			LINEV, app->ref, name
		);
		sym->ref = (GLint)glGetUniformLocation( app->ref, name );
	}

	vfxsym->used = (sym->ref >= 0);
	if ( !(vfxsym->used) )
	{
		dint err = ENOTRECOVERABLE;
		Log2VfxVaif
( XMSG_ERRNO("Was given location %ld\n",err), sym->ref );
		return err;
	}

	return 0;
}

dint libLoseFailed( VFXREF *vfxsym ) { (void)vfxsym; return ENOTSUP; }
dint libFindFailed( VFXREF *vfxapp, VFXREF *vfxsym )
	{ (void)vfxapp; (void)vfxsym; return ENOTSUP; }
dint libMarkFailed( VFXREF *vfxsym ) { (void)vfxsym; return ENOTSUP; }
dint libSendFailed( VFXREF *vfxsym ) { (void)vfxsym; return ENOTSUP; }
dint libDenyFailed( VFXREF *vfxsym ) { (void)vfxsym; return ENOTSUP; }
dint libTakeFailed( VFXREF *vfxsym ) { (void)vfxsym; return ENOTSUP; }
dint libDrawFailed( VFXREF *vfxsym, VFXDRAW mode, uint occurs )
	{ (void)vfxsym; (void)mode; (void)occurs; return ENOTSUP; }

dint libLoseNothing( VFXREF *vfxsym ) { (void)vfxsym; return 0; }
dint libFindNothing( VFXREF *vfxapp, VFXREF *vfxsym )
	{ (void)vfxapp; (void)vfxsym; return 0; }
dint libMarkNothing( VFXREF *vfxsym ) { (void)vfxsym; return 0; }
dint libSendNothing( VFXREF *vfxsym ) { (void)vfxsym; return 0; }
dint libDenyNothing( VFXREF *vfxsym ) { (void)vfxsym; return 0; }
dint libTakeNothing( VFXREF *vfxsym ) { (void)vfxsym; return 0; }
dint libDrawNothing( VFXREF *vfxsym, VFXDRAW mode, uint occurs )
	{ (void)vfxsym; (void)mode; (void)occurs; return 0; }

dint libDenyVerticeBoundVfxSym( VFXREF *vfxsym )
{
	LIB_VFXREF *cfg = vfxsym->bound->libref;
	LIB_VFXREF *sym = vfxsym->libref;
	Log2VfxVaif
	(
		LINEF "glDisableVertexArrayAttrib( %lu, %lu );\n",
		LINEV, cfg->ref, sym->ref
	);
	glDisableVertexArrayAttrib( cfg->ref, sym->ref );
	return 0;
}

dint libTakeVerticeBoundVfxSym( VFXREF *vfxsym )
{
	LIB_VFXREF *cfg = vfxsym->bound->libref;
	LIB_VFXREF *sym = vfxsym->libref;
	Log2VfxVaif
	(
		LINEF "glEnableVertexArrayAttrib(  %lu, %lu );\n",
		LINEV, cfg->ref, sym->ref
	);
	glEnableVertexArrayAttrib( cfg->ref, sym->ref );
	return 0;
}

#define _FUNC( T, SFX, ARGS, ... ) \
dint libSendGlobal##SFX( VFXREF *vfxsym, void *vec ) \
{ \
	T *v = vec; \
	LIB_VFXREF *sym = vfxsym->libref; \
	Log2VfxVaif \
	( \
		LINEF "glUniform" #SFX "( %ld, " ARGS " );\n", \
		LINEV, sym->ref, __VA_ARGS__ \
	); \
	glUniform##SFX( sym->ref, __VA_ARGS__ ); \
	return 0; \
}
#define FUNC( T, SFX, EACH ) \
_FUNC( T, 1##SFX, EACH, v[0] ) \
_FUNC( T, 2##SFX, EACH ", " EACH, v[0], v[1] ) \
_FUNC( T, 3##SFX, EACH ", " EACH ", " EACH, v[0], v[1], v[2] ) \
_FUNC( T, 4##SFX, EACH ", " EACH ", " EACH ", " EACH, v[0], v[1], v[2], v[3] )

FUNC( uint, ui, "%u" )
FUNC( dint,  i, "%d" )
FUNC( fnum,  f, "%f" )
FUNC( dnum,  d, "%f" )

#undef FUNC
#undef _FUNC

dint libMarkVerticeBoundVfxSym( VFXREF *vfxsym )
{
	LIB_VFXREF *sym = vfxsym->libref;
	VFXDEF *vfxdef = vfxsym->vfxdef;
	uptr offset = vfxsym->offset;
	ulong	type = libVfxDataTypes[ vfxdef->type ];
	achs	name = libVfxDataEnums[ vfxdef->type ];
	VFXREF *vfxbuf = vfxsym;
	while ( vfxbuf->buff == VFXBUFF_INDICE_INPUT )
		vfxbuf = vfxbuf->bound;
	if ( vfxsym == vfxbuf )
	{
		Log2VfxVaif
		(
			LINEF "glVertexAttribPointer"
			"( %ld, %u, %s, GL_FALSE, %zu, %p );\n",
			LINEV, sym->ref, vfxdef->num, name, vfxdef->bytes,
			(void*)offset
		);
		glVertexAttribPointer
		(
			sym->ref, vfxdef->num, type,
			GL_FALSE, vfxdef->bytes, NULL
		);
		return 0;
	}
	return 0;
}

#define _FUNC( PFX ) \
dint libSendGlobal##PFX##v( VFXREF *vfxsym ) \
{ \
	LIB_VFXREF *sym = vfxsym->libref; \
	VFXDEF *vfxdef = vfxsym->vfxdef; \
	BUFFER *buffer = vfxsym->Buff; \
	Log2VfxVaif \
	( \
		LINEF "glUniform" #PFX "v(  %lu, %u, %p );\n", \
		LINEV, sym->ref, vfxdef->num, buffer->array \
	); \
	glUniform##PFX##v( sym->ref, vfxdef->num, buffer->array ); \
	return 0; \
}

#define FUNC( PFX ) \
	_FUNC( 1##PFX ) _FUNC( 2##PFX ) _FUNC( 3##PFX ) _FUNC( 4##PFX )


FUNC( ui )
FUNC( i )
FUNC( f )
FUNC( d )

#undef FUNC
#undef _FUNC

dint libSendUniformValueVfxSym( VFXREF *vfxsym )
{
	uchar *data = NULL;
	if ( !(vfxsym->bound) )
		data = SeekMembers( vfxsym->Buff );
	else
	{
		data = SeekMembers( vfxsym->bound->Buff );
		data = data + (vfxsym->offset);
	}
	return libVfxVai->sendGlobalCB[vfxsym->vfxdef->type]( vfxsym, data );
}

dint libMarkVerticeIndexVfxSym( VFXREF *vfxsym )
{
	VFXREF *vfxcfg = vfxsym->bound->bound;
	LIB_VFXREF *sym = vfxsym->libref;
	LIB_VFXREF *cfg = vfxcfg->libref;
	libBindVfxBuf( vfxsym );
	Log2VfxVaif
	(
		LINEF "glVertexArrayElementBuffer( %ld, %ld );\n",
		LINEV, cfg->ref, sym->buf
	);
	glVertexArrayElementBuffer( cfg->ref, sym->buf );
	return 0;
}

dint libDrawVerticeIndexVfxSym( VFXREF *vfxsym, VFXDRAW mode, uint occurs )
{
	BUFFER *buffer = vfxsym->Buff;
	ulong	vtype = libVfxDataTypes[vfxsym->vfxdef->type];
	achs	vname = libVfxDataEnums[vfxsym->vfxdef->type];
	ulong	dtype = libVfxDrawTypes[mode];
	achs	dname = libVfxDrawEnums[mode];
	uint	count = buffer->num;
	if ( occurs )
	{
		Log2VfxVaif
		(
LINEF "glDrawElementsInstanced( %s, %u, %s, %p, %u );\n",
LINEV, dname, count, vname, buffer->array, occurs
		);
		glDrawElementsInstanced
			( dtype, count, vtype, buffer->array, occurs );
	}
	else
	{
		Log2VfxVaif
		(
			LINEF "glDrawElements( %s, %u, %s, %p );\n",
			LINEV, dname, count, vname, NULL
		);
		glDrawElements( dtype, count, vtype, NULL );
	}
	return 0;
}

/* Vertex Input */
dint libDrawVerticeBoundVfxSym( VFXREF *vfxsym, VFXDRAW mode, uint occurs )
{
	ulong draw = libVfxDrawTypes[mode];
	achs dname = libVfxDrawEnums[mode];
	uint count = vfxsym->Buff->num;
	if ( occurs )
	{
		Log2VfxVaif
		(
			LINEF "glDrawArraysInstanced( %s, %u, %u, %u );\n",
			LINEV, dname, 0, count, occurs
		);
		glDrawArraysInstanced( draw, 0, count, occurs );
	}
	else
	{
		Log2VfxVaif
		(
			LINEF "glDrawArrays( %s, %u, %u );\n",
			LINEV, dname, 0, count
		);
		glDrawArrays( draw, 0, count );
	}
	return 0;
}

dint libLinkVfxCfgVarSymbols( VFXVAI *vfxvai )
{
	VFXDEST i;

	vfxvai->initVfxRefCB[VFXTYPE_SYMBOL] = libInitVfxSym;
	vfxvai->termVfxRefCB[VFXTYPE_SYMBOL] = libTermVfxSym;

	for ( i = 0; i < VFXDEST_COUNT; ++i )
	{
		vfxvai->loseVfxSymCB[i] = libLoseFailed;
		vfxvai->findVfxSymCB[i] = libFindFailed;
		vfxvai->markVfxSymCB[i] = libMarkFailed;
		vfxvai->takeVfxSymCB[i] = libTakeFailed;
		vfxvai->drawVfxSymCB[i] = libDrawFailed;
		vfxvai->denyVfxSymCB[i] = libDenyFailed;
	}

	i = VFXDEST_UNIFORM_ARRAY;
	vfxvai->loseVfxSymCB[i] = libLoseVfxSym;
	vfxvai->findVfxSymCB[i] = libFindUniformArrayVfxSym;
	vfxvai->markVfxSymCB[i]	= libMarkNothing;
	vfxvai->sendVfxSymCB[i] = libSendVfxSymBuffer;
	vfxvai->takeVfxSymCB[i]	= libTakeNothing;
	vfxvai->drawVfxSymCB[i] = libDrawNothing;
	vfxvai->denyVfxSymCB[i]	= libDenyNothing;

	i = VFXDEST_UNIFORM_BLOCK;
	vfxvai->loseVfxSymCB[i] = libLoseVfxSym;
	vfxvai->findVfxSymCB[i] = libFindUniformBlockVfxSym;
	vfxvai->markVfxSymCB[i]	= libMarkNothing;
	vfxvai->sendVfxSymCB[i]	= libSendNothing;
	vfxvai->takeVfxSymCB[i]	= libTakeNothing;
	vfxvai->drawVfxSymCB[i] = libDrawNothing;
	vfxvai->denyVfxSymCB[i]	= libDenyNothing;

	i = VFXDEST_UNIFORM_VALUE;
	vfxvai->loseVfxSymCB[i] = libLoseVfxSym;
	vfxvai->findVfxSymCB[i] = libFindUniformValueVfxSym;
	vfxvai->markVfxSymCB[i] = libMarkNothing;
	vfxvai->sendVfxSymCB[i]	= libSendUniformValueVfxSym;
	vfxvai->takeVfxSymCB[i]	= libTakeNothing;
	vfxvai->drawVfxSymCB[i] = libDrawNothing;
	vfxvai->denyVfxSymCB[i]	= libDenyNothing;

	i = VFXDEST_VERTICE_BOUND;
	vfxvai->loseVfxSymCB[i] = libLoseVfxSym;
	vfxvai->findVfxSymCB[i] = libFindVerticeBoundVfxSym;
	vfxvai->markVfxSymCB[i]	= libMarkVerticeBoundVfxSym;
	vfxvai->sendVfxSymCB[i] = libSendVfxSymBuffer;
	vfxvai->takeVfxSymCB[i]	= libTakeVerticeBoundVfxSym;
	vfxvai->denyVfxSymCB[i]	= libDenyVerticeBoundVfxSym;

	i = VFXDEST_VERTICE_INDEX;
	vfxvai->loseVfxSymCB[i] = libLoseVfxSym;
	vfxvai->findVfxSymCB[i] = libFindVerticeBoundVfxSym;
	vfxvai->markVfxSymCB[i]	= libMarkVerticeIndexVfxSym;
	vfxvai->sendVfxSymCB[i] = libSendVfxSymBuffer;
	vfxvai->takeVfxSymCB[i]	= libTakeVerticeBoundVfxSym;
	vfxvai->drawVfxSymCB[i] = libDrawVerticeIndexVfxSym;
	vfxvai->denyVfxSymCB[i]	= libDenyVerticeBoundVfxSym;

	vfxvai->bindVfxSrcCB[VFXTYPE_SYMBOL	] = libBindVfxBufSource;

	vfxvai->sendGlobalCB[VFXDATA_UINT	] = libSendGlobal1ui;
	vfxvai->sendGlobalCB[VFXDATA_UVEC2	] = libSendGlobal2ui;
	vfxvai->sendGlobalCB[VFXDATA_UVEC3	] = libSendGlobal3ui;
	vfxvai->sendGlobalCB[VFXDATA_UVEC4	] = libSendGlobal4ui;

	vfxvai->sendGlobalCB[VFXDATA_DINT	] = libSendGlobal1i;
	vfxvai->sendGlobalCB[VFXDATA_IVEC2	] = libSendGlobal2i;
	vfxvai->sendGlobalCB[VFXDATA_IVEC3	] = libSendGlobal3i;
	vfxvai->sendGlobalCB[VFXDATA_IVEC4	] = libSendGlobal4i;

	vfxvai->sendGlobalCB[VFXDATA_FNUM	] = libSendGlobal1f;
	vfxvai->sendGlobalCB[VFXDATA_FVEC2	] = libSendGlobal2f;
	vfxvai->sendGlobalCB[VFXDATA_FVEC3	] = libSendGlobal3f;
	vfxvai->sendGlobalCB[VFXDATA_FVEC4	] = libSendGlobal4f;

	vfxvai->sendGlobalCB[VFXDATA_DNUM	] = libSendGlobal1d;
	vfxvai->sendGlobalCB[VFXDATA_DVEC2	] = libSendGlobal2d;
	vfxvai->sendGlobalCB[VFXDATA_DVEC3	] = libSendGlobal3d;
	vfxvai->sendGlobalCB[VFXDATA_DVEC4	] = libSendGlobal4d;

	vfxvai->sendGlobalvCB[VFXDATA_UINT	] = libSendGlobal1uiv;
	vfxvai->sendGlobalvCB[VFXDATA_UVEC2	] = libSendGlobal2uiv;
	vfxvai->sendGlobalvCB[VFXDATA_UVEC3	] = libSendGlobal3uiv;
	vfxvai->sendGlobalvCB[VFXDATA_UVEC4	] = libSendGlobal4uiv;

	vfxvai->sendGlobalvCB[VFXDATA_DINT	] = libSendGlobal1iv;
	vfxvai->sendGlobalvCB[VFXDATA_IVEC2	] = libSendGlobal2iv;
	vfxvai->sendGlobalvCB[VFXDATA_IVEC3	] = libSendGlobal3iv;
	vfxvai->sendGlobalvCB[VFXDATA_IVEC4	] = libSendGlobal4iv;

	vfxvai->sendGlobalvCB[VFXDATA_FNUM	] = libSendGlobal1fv;
	vfxvai->sendGlobalvCB[VFXDATA_FVEC2	] = libSendGlobal2fv;
	vfxvai->sendGlobalvCB[VFXDATA_FVEC3	] = libSendGlobal3fv;
	vfxvai->sendGlobalvCB[VFXDATA_FVEC4	] = libSendGlobal4fv;

	vfxvai->sendGlobalvCB[VFXDATA_DNUM	] = libSendGlobal1dv;
	vfxvai->sendGlobalvCB[VFXDATA_DVEC2	] = libSendGlobal2dv;
	vfxvai->sendGlobalvCB[VFXDATA_DVEC3	] = libSendGlobal3dv;
	vfxvai->sendGlobalvCB[VFXDATA_DVEC4	] = libSendGlobal4dv;

	return 0;
}
