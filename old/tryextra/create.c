#include "main.h"

dint create( APP *app )
{
	vec3s one3 = {{1,1,1}};
	uniforms *Uniforms = NULL;
	vec4s nulls[] =
	{
#if 1
		{{0}}, {{0}}, {{0}}, {{0}}, {{0}}, {{0}}
#else
		{{0}},
		{{1,1,1,1}},
		{{-0.75, -0.75, 0, 0 }},
		{{-0.75,  0.75, 0, 0 }},
		{{ 0.75, -0.75, 0, 0 }},
		{{ 0.75,  0.75, 0, 0 }}
#endif
	};

	uint	Indices[] = {0,0,0,0,0,0};
#define INDICES_COUNT (sizeof(_VertexNullNames) / sizeof(achs))

	achs	UniformNames[] = { UNIFORM_NAMES };
#define UNIFORM_NAMEC (sizeof(UniformNames) / sizeof(achs))
#define COUNT UNIFORM_NAMEC
	VFXDATA	UniformDataTypes[COUNT	] = { UNIFORM_TYPES };
	uint	UniformsTake[COUNT	] = { UNIFORMS_TAKE };
#undef COUNT

	OBJECT *config = NULL, *object = NULL, *symbol = NULL, *prvsib = NULL;
	VFXREF *vfxcfg = NULL, *vfxref = NULL, *vfxsym = NULL;
	VFXREF *vfxapp = app->vfxapp;
	BUFFER *buffer = NULL;
	dint err = 0;
	void *data = NULL;
	uint i, size;

	/* Create initialise the main configuration */
	err = MakeVfxRefb( app, &vfxcfg, "Test Config" );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	MarkVfxRefBound( vfxcfg, vfxcfg );

	app->vfxcfg = vfxcfg;
	err = InitVfxCfg( vfxcfg );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	BindVfxCfg( vfxcfg );
	/* Initialise main buffer */
	err = MarkVfxRefDataType( vfxcfg, VFXDATA_FVEC4 );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	err = InitVfxBuf
	(
		vfxcfg,
		VFXBUFF_SYMBOL_INPUT,
		VFXHINT_LASTING_DRAW
	);
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	buffer = SeekVfxBufData( vfxcfg );
	err = MarkBuffern( buffer, nulls, sizeof(nulls) / sizeof(nulls[0]) );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	BindVfxBuf( vfxcfg );
	SendVfxBuf( vfxcfg );
	config = SeekVfxRefNode( vfxcfg );

	/* Create & initialise the uniforms */
	DEBUG_EXEC
	(
		Printf
		(
			"====================================="
			"'UNIFORMS' name count = %u\n", UNIFORM_NAMEC
		);
	);

	err = MakeVfxRefb( app, &vfxref, "UNIFORMS" );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	object = SeekVfxRefNode( vfxref );
	MarkParent( object, config );
	err = BindObjKid( config, NULL, object );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	err = MarkVfxRefDataType( vfxref, VFXDATA_UCHAR );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	err = InitVfxBuf( vfxref, 0, 0 );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	buffer = SeekVfxBufData( vfxref );
	err = MarkBufferc( buffer, sizeof(uniforms) );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	err = FindVfxSym( vfxapp, vfxref, VFXDEST_UNIFORM_BLOCK );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	app->Uniforms = Uniforms = SeekMembers( buffer );
	memset( Uniforms, 0, sizeof(uniforms) );

	Uniforms->WinSpace.x = 480;
	Uniforms->WinSpace.y = 640;
	/* A depth of 10,000 should be enough for normal game play */
	Uniforms->WinSpace.z = 10000;

	Uniforms->VtxCount = 4;
	Uniforms->FlatLine = 1;

	Uniforms->WinScale = one3;
	Uniforms->WinScale.y = 480.0 / 640.0;

	Uniforms->RegScale = one3;
	Uniforms->RegEmits = one3;

	for ( i = 0; i < UNIFORM_NAMEC; ++i )
	{
		VFXDATA DataType = UniformDataTypes[i];
		VFXDEF *vfxdef = SeekVfxDef( DataType );
		achs name = UniformNames[i];
		uint takes = UniformsTake[i];

		DEBUG_EXEC
		(
			Printf
			(
				"------------------------- [%d] %s %s",
				i, vfxdef->named, name
			);
			if ( takes > 1 )
				Printf( "[%u]\n", takes );
			else
				Putchar( '\n' );
		);

		if ( !(vfxdef->count) )
			return err;

		vfxsym = NULL;
		err = MakeVfxRefb( app, &vfxsym, name );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			return err;
		}

		symbol = SeekVfxRefNode( vfxsym );
		MarkVfxRefBound( vfxsym, vfxref );
		MarkParent( symbol, object );
		MarkPrvSib( symbol, prvsib );
		err = BindObjKid( object, NULL, symbol );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			return err;
		}

		err = MarkVfxSymTakes( vfxsym, takes );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			return err;
		}

		err = MarkVfxRefDataType( vfxsym, DataType );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			return err;
		}

		err = FindVfxSym( vfxapp, vfxsym, VFXDEST_UNIFORM_VALUE );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			Printf( "Couldn't find '%s'\n", name );
			return err;
		}

		err = MarkVfxSym( vfxsym );
		if ( err )
		{
			Printf( XMSG_ERRNO(,err) );
			return err;
		}

		prvsib = symbol;
		SeekVfxRefLog( &(app->Text), NULL );
		Printf( "%.*s\n", app->Text.count, app->Text.array );
	}

	DEBUG_EXEC
	(
		Printf
		(
			"====================================="
			"vec4 NulPoint, name count = %u\n",
			sizeof(Indices) / sizeof(uint)
		);
	);

	vfxsym = NULL;
	err = MakeVfxRefb( app, &vfxsym, "NulPoint" );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	err = MarkVfxRefDataType( vfxsym, VFXDATA_UINT );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}
	err = InitVfxBuf
	(
		vfxsym,
		VFXBUFF_INDICE_INPUT,
		VFXHINT_LASTING_DRAW
	);
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	data = Indices;
	buffer = SeekVfxBufData( vfxsym );
	Uniforms->VtxCount = size = sizeof(Indices) / sizeof(uint);
	err = MarkBuffern( buffer, data, size );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	BindVfxBuf( vfxsym );
	SendVfxBuf( vfxsym );

	symbol = SeekVfxRefNode( vfxsym );
	err = BindObjKid( config, NULL, symbol );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	MarkParent( symbol, config );
	MarkVfxRefBound( vfxsym, vfxcfg );
	err = FindVfxSym( vfxapp, vfxsym, VFXDEST_VERTICE_INDEX );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

	err = MarkVfxSym( vfxsym );
	if ( err )
	{
		Printf( XMSG_ERRNO(,err) );
		return err;
	}

#undef INDICES_NAMEC
	return err;
}
