#include "main.h"
#include <basic/buffer/_/_text.h>

APP app = {0};

dint changeColors() { return 0; }
void void_viewfx()
{
	TermVfxDbg( true );
	VoidVfxRefs( &app );
	app.Uniforms = NULL;
	app.vfxcfg = NULL;
	app.vfxapp = NULL;
	TermVfxVai( app.vai.hook );
	VoidVfxWin( &(app.vfxwin) );
	TermVfxWai( app.wai.hook );
}
void void_basic()
{
	ACHS *Text = &(app.Text);
	ShutLib( &(app.vai) );
	ShutLib( &(app.wai) );
	VoidAchs( &(app.vai.File) );
	VoidAchs( &(app.wai.File) );
	SeekVfxRefLog( Text, NULL );
	Printf( "%.*s\n", Text->count, Text->array );
	SeekVfxWinLog( Text, NULL );
	Printf( "%.*s\n", Text->count, Text->array );
	TermAchs( &(app.Text) );
	VoidAllObjects();
}

void cleanup()
{
	void_viewfx();
	clearenv();
	void_basic();
}

dint PrepVfxWai( APP *app );
dint PrepVfxVai( APP *app );

int main( int argc, char *argv[] )
{
	dint err = 0, one = 0, two = 0;
	atexit( cleanup );
	setbuf( stdout, NULL );
	InitVfxTypeEnums();
	InitVfxDataEnums();
	InitVfxBuffEnums();
	InitVfxHintEnums();
	do
	{
		achs name = NULL, hint = NULL, tok = NULL;
		ACHS *Text = NULL, *Path = NULL;
		dint i, winX, winY, winH, winW;
		uniforms *Uniforms = NULL;
		vec4s *nulls = NULL;
		double scale;
		_Bool printed_draw = false;

		app.wai.name = "glfw";
		app.vai.name = "gl";

		for ( i = 0; i < argc; ++i )
		{
			dmax wai_diff = 0, vai_diff = 0;
			ACHS *Name = NULL;

			name = argv[i];

			if ( *name != '-' )
				continue;

			tok = achschar( argv[i], '=' );

			if ( !tok )
				hint = argv[++i];
			else
			{
				*((ach*)tok) = 0;
				hint = tok + 1;
			}

			wai_diff = achsdiff( name, "--wai" );
			vai_diff = achsdiff( name, "--vai" );

			if ( tok )
				*((ach*)tok) = '=';

			if ( hint && wai_diff == 0 )
				app.wai.name = hint;

			if ( hint && vai_diff == 0 )
				app.vai.name = hint;

			if ( name[1] == 'D' )
			{
				if ( name[2] )
					name += 2;
				else if ( argv[i] != name )
					name = argv[i];
				else
					name = argv[++i];

				tok = achschar( name, '=' );
				if ( !tok )
					hint = (argv[i] == name) ? argv[++i] : argv[i];
				else
				{
					*((ach*)tok) = 0;
					hint = tok + 1;
				}

				err = MakeAchsb( &app, &Name, name );
				if ( err )
				{
					Printf( XMSG_ERRNO(,err) );
					goto fail;
				}

				setenv( name, hint, true );

				if ( tok )
					*((ach*)tok) = '=';
			}
		}

		one = MakeAchsf
		(
			&app, &(app.wai.File),
			"./" DLL_PFX "vfx%s" DLL_SFX, app.wai.name
		);

		two = MakeAchsf
		(
			&app, &(app.vai.File),
			"./" DLL_PFX "vfx%s" DLL_SFX, app.vai.name
		);

		name = getenv("APP_DATA");
		hint = "/";
		if ( !name )
			hint = name = "";

		Text = &(app.Text);
		app.usual.ud = &app;
		Text->usual = &(app.usual);
		Path = &(app.Path);
		Path->usual = &(app.usual);
		err = InitAchs( Text );
		if ( one || two || err )
		{
			if ( one )
				Printf( XMSG_ERRNO(,one) );
			if ( two )
				Printf( XMSG_ERRNO(,two) );
			break;
		}

		app.wai.file = SeekAchsb( app.wai.File );
		app.vai.file = SeekAchsb( app.vai.File );

		one = OpenLib( &app, &(app.wai), PrepVfxWai );
		two = OpenLib( &app, &(app.vai), PrepVfxVai );

		if ( one || two )
		{
			if ( one )
				Printf
( XMSG_ERRNO( "Failed to open vfxwai module '%s'\n", one ), app.wai.file );
			if ( two )
				Printf
( XMSG_ERRNO( "Failed to open vfxvai module '%s'\n", two ), app.vai.file );
			break;
		}

		app.err = SeekVfxWinSize( app.vfxwin, &winH, &winW );
		if ( app.err )
		{
			Printf( XMSG_ERRNO(,app.err) );
			winH = 640;
			winW = 480;
		}

		app.err = SeekVfxWinPos( app.vfxwin, &winX, &winY );
		if ( app.err )
		{
			Printf( XMSG_ERRNO(,app.err) );
			winX = 0;
			winY = 0;
		}

		nulls = SeekMembers( SeekVfxBufData(app.vfxcfg) );
		Uniforms = app.Uniforms;

		app.err = SeekVfxWinSize( app.vfxwin, &winH, &winW );
		if ( app.err )
			Printf( XMSG_ERRNO(,app.err) );

		app.err = SeekVfxWinPos( app.vfxwin, &winX, &winY );
		if ( app.err )
			Printf( XMSG_ERRNO(,app.err) );

		Uniforms->VtxRedos = 6;
		Uniforms->WinSpace.x = winW;
		Uniforms->WinSpace.y = winH;
		Uniforms->WinPoint.x = winX;
		Uniforms->WinPoint.y = winY;

		scale = ((double)winW) / ((double)winH);
		Uniforms->WinScale.x = (scale < 1.0) ? scale : 1.0;
		Uniforms->WinScale.y = (scale > 1.0) ? scale : 1.0;

		(void)nulls;
		SeekVfxRefLog( Text, NULL );
		Printf( "%.*s\n", Text->count, Text->array );

		DEBUG_EXEC( Puts("===========Entering draw loop=========="); );

		while ( !DeadVfxWin( app.vfxwin ) )
		{
			BindVfxWin( app.vfxwin );
			BindVfxApp( app.vfxapp );
			MarkVfxBox( 0.0, 0.0, 0.0, 1.0 );
			ZeroVfxBit( VFX_COLOR_BIT | VFX_DEPTH_BIT );

			if ( BusyVfxKey( app.vfxwin, VFXKEY_ANSI_ESC ) )
				KillVfxWin( app.vfxwin );

			app.err = SeekVfxWinSize( app.vfxwin, &winH, &winW );
			if ( app.err )
				Printf( XMSG_ERRNO(,app.err) );

			app.err = SeekVfxWinPos( app.vfxwin, &winX, &winY );
			if ( app.err )
				Printf( XMSG_ERRNO(,app.err) );

			Uniforms->WinSpace.x = winW;
			Uniforms->WinSpace.y = winH;
			Uniforms->WinPoint.x = winX;
			Uniforms->WinPoint.y = winY;

			app.err = MoveVfxBox( 0, 0, winH, winW );
			if ( app.err )
				Printf( XMSG_ERRNO(,app.err) );

			err = DrawVfxCfg
			(
				app.vfxcfg,
				VFXDRAW_TRIO_BELT,
				Uniforms->VtxRedos
			);
			if ( err )
			{
				Printf( XMSG_ERRNO(,err) );
				break;
			}

			SwapVfxWinBufs( app.vfxwin );
			PollVfxWai();

			SeekVfxRefLog( Text, NULL );
			if ( !printed_draw )
			{
				Printf( "%.*s\n", Text->count, Text->array );
				printed_draw = true;
			}
		}

		DEBUG_EXEC( Puts("===========Exited draw loop============"); );
	}
	while (0);
	fail:
	cleanup();
	app.err = one ? one : (two ? two : err);
	return app.err ? EXIT_FAILURE : EXIT_SUCCESS;
}

dint PrepVfxWaiInitVfxWai( APP *app )
{
	if ( achsfind( app->vai.name, "gl" ) )
	{
		achs init_vfxwin_args[] =
		{
			"-l", app->vai.name,
			"--major=4",
			"--minor=3",
			"--samples=4",
			"--foreward-compatability=1",
			"--profile=core",
			NULL
		};

		return InitVfxWai( app, app->wai.hook, init_vfxwin_args );
	}

	return ENOTSUP;
}
dint PrepVfxWaiMakeVfxWin( APP *app )
	{ return MakeVfxWin( app, &(app->vfxwin) ); }
dint PrepVfxWaiOpenVfxWin( APP *app )
	{ return OpenVfxWin( app->vfxwin, "Test", 640, 480 ); }
dint PrepVfxWaiBindVfxWin( APP *app )
	{ return BindVfxWin( app->vfxwin ); }
dint PrepVfxWai( APP *app )
{
	int i, one, two;
	ACHS *Text = &(app->Text);
	PrepFunc_cb preps[] =
	{
		PrepVfxWaiInitVfxWai,
		PrepVfxWaiMakeVfxWin,
		PrepVfxWaiOpenVfxWin,
		PrepVfxWaiBindVfxWin,
		NULL
	};

	for ( i = 0; preps[i]; ++i )
	{
		one = preps[i]( app );
		two = SeekVfxWinLog( Text, NULL );
		if ( two == 0 )
			Printf( "%.*s", Text->count, Text->array );
		app->err = one ? one : two;
		if ( app->err )
		{
			Printf( XMSG_ERRNO(,app->err) );
			return app->err;
		}
	}

	return 0;
}

dint PrepVfxVaiInitVfxVai( APP *app )
	{ return InitVfxVai( app, app->vai.hook ); }
dint PrepVfxVaiMakeVfxRef( APP *app )
	{ return MakeVfxRef( app, &(app->vfxapp) ); }
dint PrepVfxVaiInitVfxDbg( APP *app )
	{ return InitVfxDbg( true, debugVfx, app ); }
dint PrepVfxVaiInitVfxApp( APP *app )
	{ return InitVfxApp( app->vfxapp, SeekAchsb(&(app->Path)), NULL ); }
dint PrepVfxVaiBindVfxApp( APP *app )
	{ return BindVfxApp( app->vfxapp ); }
dint PrepVfxVai( APP *app )
{
	achs sep = "/";
	int i, one, two;
	ACHS *Text = &(app->Text);
	ACHS *Path = &(app->Path);
	achs dir = getenv("APP_DATA");
	PrepFunc_cb preps[] =
	{
		PrepVfxVaiInitVfxVai,
		PrepVfxVaiMakeVfxRef,
		PrepVfxVaiInitVfxDbg,
		PrepVfxVaiInitVfxApp,
		PrepVfxVaiBindVfxApp,
		create,
		NULL
	};

	if ( !dir )
		sep = dir = "";
	one = MarkAchsf( Path, "%s%sshaders/vfxapps.ini", dir, sep );
	if ( one )
	{
		Printf( XMSG_ERRNO(,one) );
		return one;
	}

	for ( i = 0; preps[i]; ++i )
	{
		one = preps[i]( app );
		two = SeekVfxRefLog( Text, NULL );
		if ( two == 0 )
			Printf( "%.*s", Text->count, Text->array );
		app->err = one ? one : two;
		if ( app->err )
		{
			Printf( XMSG_ERRNO(,app->err) );
			return one;
		}
	}
	return 0;
}

