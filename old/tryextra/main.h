#ifndef INC_MAIN_H
#define INC_MAIN_H

#include <extra/viewfx/vfxwin.h>
#include <extra/viewfx/vfxapi.h>
#include <extra/viewfx/vfxref.h>
#include <cglm/struct.h>
#include <cglm/cglm.h>
#include <cglm/call.h>

dint nopause( void *ud );

#ifdef ON_WINDOWS
#define DLL_PFX
#define DLL_SFX "32.dll"
#elif defined(_Darwin)
#define DLL_PFX "lib"
#define DLL_SFX ".dylib"
#else
#define DLL_PFX "lib"
#define DLL_SFX ".so"
#endif

#include "../../run/shaders/same.glsl"

typedef struct _LIB
{
	void*	lock;
	achs	name;
	ACHS*	File;
	achs	file;
	MODULE*	hook;
} LIB;

typedef enum _GFXBUFS
{
	GFXBUF_NONE = 0,
	GFXBUF_UINTS,
	GFXBUF_DINTS,
	GFXBUF_FNUMS,
	GFXBUF_DNUMS,
	GFXBUF_COUNT
} APP_BUF;

typedef struct _APP
{
	dint err;
	VFXWIN *vfxwin;
	VFXREF *vfxapp;
	VFXREF *vfxcfg;
	uniforms *Uniforms;
	LIB wai, vai;
	COMMON usual;
	ACHS Text;
	ACHS Path;
} APP;

typedef dint (*PrepFunc_cb)( APP *app );
dint OpenLib( APP *app, LIB *lib, PrepFunc_cb PrepFunc );
void ShutLib( LIB *lib );

dint debugVfx
(
	dint source,
	dint defect,
	uint seekid,
	dint weight,
	dcap length,
	achs report,
	const void *ud
);
dint create( APP *app );
dint events( APP *app );
void void_shared( APP *app );

#endif
