OBJS:=$(CFILES:%=%.shared.o)
CODE:=$(CFILES:%=%.shared.i)
CFLAGS+=-std=$(STDC)

vpath %.so %.dylib %.dll $(TDIR)
vpath %.shared.o %.shared-mac.o %.shared32.o %.shared64.o %.o $(ODIR)

all: lib$(NAME).so lib$(NAME).dylib $(NAME)32.dll $(NAME)64.dll

lib$(NAME).so: $(OBJS)
	$(call link,CC,-shared $(build_linux),$@,,$(OBJS),$(call link_libs,))

lib$(NAME).dylib: $(OBJS:%.o=%-mac.o)
	$(call link,CC,-shared $(build_apple),$@,,$(OBJS:%.o=%-mac.o),$(call link_libs,))

$(NAME)32.dll: $(OBJS:%.o=%32.o)
	$(call link,CC32,-shared $(build_win32),$@,,$(OBJS:%.o=%32.o),$(call link_libs,32))

$(NAME)64.dll: $(OBJS:%.o=%64.o)
	$(call link,CC64,-shared $(build_win64),$@,,$(OBJS:%.o=%64.o),$(call link_libs,64))

%.c.shared.o: %.c
	$(call compile,CC,-shared $(CFLAGS) $(build_linux) -Wfatal-errors,shared,$@,$<)

%.c.shared.i: %.c
	$(call compile,CC,-shared $(CFLAGS) $(build_linux) -E,shared,$@,$<)

%.c.shared-mac.o: %.c
	$(call compile,CC,-shared -Wfatal-errors $(CFLAGS) $(build_apple),shared,$@,$<)

%.c.shared32.o: %.c
	$(call compile,CC32,-shared -Wfatal-errors $(CFLAGS) $(build_win32),shared,$@,$<)

%.c.shared64.o: %.c
	$(call compile,CC64,-shared -Wfatal-errors $(CFLAGS) $(build_win64),shared,$@,$<)
