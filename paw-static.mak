OBJS:=$(CFILES:%=%.static.o)

vpath %.a $(TDIR)
vpath %.static.o %.static-mac.o %.static32.o %.static64.o %.o $(ODIR)

all: lib$(NAME).a lib$(NAME)-mac.a $(NAME)32.a $(NAME)64.a

lib$(NAME).a: $(OBJS)
	$(call link,build_a,,$@,,$(OBJS))

lib$(NAME)-mac.a: $(OBJS:%.o=%-mac.o)
	$(call link,build_a,,$@,,$(OBJS:%.o=%-mac.o))

$(NAME)32.a: $(OBJS:%.o=%32.o)
	$(call link,build_a,,$@,,$(OBJS:%.o=%32.o))

$(NAME)64.a: $(OBJS:%.o=%64.o)
	$(call link,build_a,,$@,,$(OBJS:%.o=%64.o))

%.c.static.o: %.c
	$(call compile,CC,$(CFLAGS) $(build_linux),static,$@,$<)

%.c.static-mac.o: %.c
	$(call compile,CC,$(CFLAGS) $(build_apple),static,$@,$<)

%.c.static32.o: %.c
	$(call compile,CC32,$(CFLAGS) $(build_win32),static,$@,$<)

%.c.static64.o: %.c
	$(call compile,CC64,$(CFLAGS) $(build_win64),static,$@,$<)
