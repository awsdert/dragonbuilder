OBJS:=$(CFILES:%=%.usrapp.o)

vpath %.elf %.AppImage %.app %.exe $(TDIR)
vpath %.usrapp.o %.usrapp-mac.o %.usrapp32.o %.usrapp64.o %.o $(ODIR)

all: $(NAME).elf $(NAME).AppImage $(NAME).app $(NAME)-32.exe $(NAME)-64.exe

$(NAME).elf $(NAME).AppImage: $(OBJS)
	$(call link,CC,,$@,,$(OBJS),$(call link_libs,))

$(NAME).app: $(OBJS:%.o=%-mac.o)
	$(call link,CC,,$@,,$(OBJS:%.o=%-mac.o),$(call link_libs,))

$(NAME)-32.exe: $(OBJS:%.o=%32.o)
	$(call link,CC32,,$@,,$(OBJS:%.o=%32.o),$(call link_libs,32))

$(NAME)-64.exe: $(OBJS:%.o=%64.o)
	$(call link,CC64,,$@,,$(OBJS:%.o=%64.o),$(call link_libs,64))

%.c.usrapp.o: %.c
	$(call compile,CC,$(CFLAGS) $(build_linux),usrapp,$@,$<)

%.c.usrapp-mac.o: %.c
	$(call compile,CC,$(CFLAGS) $(build_apple),usrapp,$@,$<)

%.c.usrapp32.o: %.c
	$(call compile,CC32,$(CFLAGS) $(build_win32),usrapp,$@,$<)

%.c.usrapp64.o: %.c
	$(call compile,CC64,$(CFLAGS) $(build_win64),usrapp,$@,$<)
