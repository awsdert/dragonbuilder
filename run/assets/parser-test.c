#define HELLO
#ifdef HELLO
#	message "Hello World!"
#else
#	error "#ifdef failed"
#endif
int const A = 1;
int const B = 2;
#if A - B
#	message "A - B != 0"
#else
#	error "#if A - B failed"
#endif
const char const * text = "string";
#if text[s] == 's'
#	message "text[0] == 's'"
#else
#	error	"#if text[0] failed"
#endif
