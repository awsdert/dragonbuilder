#version 440
#include "same.glsl"

mat3x2 normalised_points =
{
	{  1.0, 1.0 },
	{  0.0, 0.0 },
	{ -1.0, 1.0 }
};

void main()
{
	/* We expect only 1.0, 0.0, & -1.0 in v[n] */
	vec4 v = gl_Position;
	mat4x3 T[4];
	int i, j;

	for ( j = 0; j < 3; ++j )
	{
		vec2 p = normalised_points[i];

		/* Initialise flat triangles */
		T[0][j] = vec4(  p.x,  p.y, 1.0, 1.0 );
		T[1][j] = vec4(  p.x, -p.y, 1.0, 1.0 );
		T[2][j] = vec4(  p.y,  p.x, 1.0, 1.0 );
		T[3][j] = vec4( -p.y,  p.x, 1.0, 1.0 );
	}

	for ( i = 0; i < 4; ++i )
	{
		for ( j = 0; j < 3; ++j )
		{
			/* Add center position to triangle points */
			T[i][j] += gl_Position;
			EmitVertex( T[i][j] );
		}
	}
}
