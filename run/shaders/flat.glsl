#version 440
#include "same.glsl"

layout(points) in;
layout(triangle_strip, max_vertices=256) out;

in VERTEX
{
	vec4	DyeColor;
	vec4	DyeTexel;
} got[1];

out VERTEX
{
	vec4	DyeColor;
	vec4	DyeTexel;
} add;

void emit_vertex( int i, vec4 point )
{
	point.xy *= uniforms.WinScale.xy * uniforms.RegScale.xy;
	point.xyz += uniforms.RegPoint.xyz;
	gl_Position = gl_in[i].gl_Position + point;
	add.DyeColor = got[0].DyeColor;
	add.DyeTexel = got[0].DyeTexel;
	EmitVertex();
}

void emit_square_half( int i, bool left )
{
	uint j = uint(left);
	emit_vertex( i, vec4( square_vertices[j++], 0.0, 1.0 ) );
	emit_vertex( i, vec4( square_vertices[j++], 0.0, 1.0 ) );
	emit_vertex( i, vec4( square_vertices[j++], 0.0, 1.0 ) );
}

void main()
{
	int i;
	uint v, vertices = uniforms.VtxCount, stop;
	vec4 center = vec4( 0.0, 0.0, 0.0, 1.0 );

	if ( vertices < 2 )
		vertices = 2;
	stop = vertices - 1;

	for ( i = 0; i < gl_in.length(); ++i )
	{
		for ( v = 0; v < stop; ++v )
		{
			if ( vertices == 4 )
			{
				emit_square_half(i, false);
				EndPrimitive();
				emit_square_half(i, true);
				EndPrimitive();
			}
			else
			{
				emit_vertex( i, center );
				emit_point( i, v, vertices );
				emit_point( i, v + 1, vertices );
				EndPrimitive();
			}
		}
	}
}
