#version 440 core
#include "same.glsl"

in VERTEX
{
	vec4 DyeColor;
	/* We use a vec4 so we can store our information in 1 cache, we can just use
	 * the extra values for something else, like say the index of the texel */
	vec4 DyeTexel;
} frag;

uniform sampler2D Texture;

out vec4 UseColor;

void main()
{
	vec4 ImgColor = texture( Texture, frag.DyeTexel.xy );
	UseColor = frag.DyeColor;// * ImgColor;
}
