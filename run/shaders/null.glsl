#version 440
#include "same.glsl"
in vec4 NulPoint;
out VERTEX
{
	vec4	DyeColor;
	vec4	DyeTexel;
} set;
void main()
{
	gl_Position = vec4( NulPoint.xyz, 1.0 );
	set.DyeColor = vec4(1.0,1.0,1.0,1.0);
	set.DyeTexel = vec4(1.0,1.0,1.0,1.0);
}
