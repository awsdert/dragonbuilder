#ifndef SAME_GLSL
#define SAME_GLSL

#	if defined(GL_SPIRV) || defined(GL_core_profile) || \
	defined(GL_compatibility_profile) || defined(GL_es_profile)
#		define IS_OPENGL
#	endif

#	if defined(IS_OPENGL) || defined(VULKAN)
/* Mappings to CGLM union names */
#		define uvec2s	uvec2
#		define uvec3s	uvec3
#		define uvec4s	uvec4
#		define ivec2s	ivec2
#		define ivec3s	ivec3
#		define ivec4s	ivec4
#		define vec2s	vec2
#		define vec3s	vec3
#		define vec4s	vec4
#		define dvec2s	dvec2
#		define dvec3s	dvec3
#		define dvec4s	dvec4
#	else
#		include <cglm/struct.h>
#		include <cglm/cglm.h>
#		include <cglm/call.h>
#		define uniform typedef struct
typedef unsigned int uint;
#	endif /* IS_OPENGL / VULKAN */


uniform UNIFORMS
{
	/* unsigned integers */
	/* Instance count */
	uint	VtxRedos;
	/* IdBuff/Vertices count */
	uint	VtxCount;
	/* Generating a curve or flat line */
	uint	FlatLine;
	/* signed integers */
	ivec2s	WinPoint;
	ivec3s	WinSpace;
	/* floats */
	vec3s	WinScale;
	vec3s	RegScale;
	vec3s	RegPoint;
	/* Light emitted */
	vec3s	RegEmits;
	/* Light taken before ray bounces */
	vec3s	RegTakes;
} uniforms;

#define UNIFORM_NAMES \
	"VtxRedos", "VtxCount", "FlatLine", \
	"WinPoint", "WinSpace", "WinScale", \
	"RegScale", "RegPoint", "RegEmits", "RegTakes"
#define UNIFORM_TYPES \
	VFXDATA_UINT, VFXDATA_UINT, VFXDATA_UINT, \
	VFXDATA_IVEC2, VFXDATA_IVEC3, VFXDATA_FVEC3, \
	VFXDATA_FVEC3, VFXDATA_FVEC3, VFXDATA_FVEC3, VFXDATA_FVEC3
#define UNIFORMS_TAKE 1, 1, 1, 1, 1, 1, 1, 1, 1, 1

#	ifndef IS_OPENGL
#		undef uniform
#	else
const int INT_MAX = int(  ~0u >> 1 );
const int INT_MIN = int(~(~0u >> 1));

const uint	uqtr = ~0u / 4;
const uint	uoct = ~0u / 8;
const double	dcap = double(~0u);
const double	dqtr = double(~0u / 4);

vec2 square_vertices[] =
{
	{  0.25,  0.25 },
	{  0.25,  0.75 },
	{  0.75,  0.25 },
	{  0.75,  0.75 }
};

vec4 edge_vertex( uint x, uint y )
{
	vec4 point;
	point.x = (float(double(x) / dqtr) * 2.0) - 1.0;
	point.y = (float(double(y) / dqtr) * 2.0) - 1.0;
	point.z = 1.0;
	point.w	= 1.0;
	return point;
}

vec4 calc_point( uint v, uint vertices )
{
	double aim = double(v);
	double max = double(vertices);
	uint rotate = uint((aim / max) * dcap);
	uint linear = rotate % uqtr;

	if ( uniforms.FlatLine != 0 )
		return edge_vertex( linear, uqtr - linear );
	else
	{
		uint curved = linear + (linear / 3);
		if ( linear < uoct )
			return edge_vertex( curved, uqtr - linear );
		else
			return edge_vertex( linear, uqtr - curved );
	}
}
#	endif /* IS_OPENGL */

#endif /* SAME_GLSL */
