#version 440 core
#include "same.glsl"

in vec4 NulPoint;
out VERTEX
{
	vec4 DyeColor;
	vec4 DyeTexel;
} frag;

void main()
{
	uint v = gl_InstanceID, c = uniforms.VtxRedos, stop = c - 1;
	vec4 center = vec4( 0.0, 0.0, 0.0, 1.0 );

	gl_Position	= vec4( square_vertices[v % 4], 1.0, 1.0 ) + NulPoint;
	frag.DyeColor	= vec4(1.0,1.0,float(v) / float(c),1.0);
	frag.DyeTexel	= vec4(1.0,1.0,1.0,1.0);
}
